package com.example.androidapp;


import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.security.auth.PrivateCredentialPermission;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;


public class SignatureAndVideoActivity extends Activity {

	Button SubmitButton;
	//Video Player Initialization
	private VideoView videoView=null;
	private String PATH="mnt/sdcard/bmxskills.3gp";
	private MediaPlayer mediaPlayer=null;
	private MediaController mediaController=null;

	private static String TAG="androidapp/SignatureAndVideoActivity";
	
	ImageView SignatureImageView;

	LinearLayout ButtonLinearLayout;
	LinearLayout SignatureHolderLinearLayout;

	LinearLayout VideoButtonContainer;
	LinearLayout VideoPlayerContailer;
	
	TextView DynamicTextView;
	TextView StaticTextView;
	
	ScrollView sView;
	
	Button GetSignatureButton;
	Button BackButton;
	Button CaptureVideoButton;
	Button btnReCaptureVideoButton;
	
	String Checkplayable="";
	String checkplayable="";
	String CheckSignature="";
	String result="";
	String resultp="";
	String Name="";
	String ID="";
	String Address="";
	String Phone="";
	
	TextView TextPart1TextView;
	TextView TextPatrrt2TextView;
	TextView TextPart3TextView;
	TextView CurrentDateTextView;
	TextView FullNameLastTextView;

	AlertDialog.Builder adb;

	TextView Title1;
	TextView Description1;
	TextView Title2;
	TextView Description2;
	TextView Title3;
	TextView Description3;
	TextView Title4;
	TextView Description4;

	String GetTitle1="";
	String GetTitle2="";
	String GetTitle3="";
	String GetTitle4="";
	String GetDescription1="";
	String GetDescription2="";
	String GetDescription3="";
	String GetDescription4="";
	String resultx="";
	
	String ColoredBody="";
	String normalBody="";
	String paidText="";
	String preHtml="";
	String htmlText="";
	String basicHtml="";
	String endHtml="";
	String multipleCheckString="";
	
	private Mail onlyMail;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d(TAG,"start onCreate");
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_signature_and_video);
		
		Title1=(TextView)findViewById(R.id.title1);
		//Title1.setText("");
		Bundle bundle =PublicData.publicBundle; //getIntent().getExtras();
		
		Log.d(TAG+"bundel", bundle.toString());
		
		result = bundle.getString("result");
		resultp=bundle.getString("resultp");

		Name=bundle.getString("name");
		ID=bundle.getString("id");
		Address=bundle.getString("address");
		Phone=bundle.getString("phone");
		
		Log.d(TAG, " "+resultp+"  "+ID);

		GetTitle1=bundle.getString("reviewt1");
		GetTitle2=bundle.getString("reviewt2");
		GetTitle3=bundle.getString("reviewt3");
		GetTitle4=bundle.getString("reviewt4");
		
		GetDescription1=bundle.getString("reviewd1");
		GetDescription2=bundle.getString("reviewd2");
		GetDescription3=bundle.getString("reviewd3");
		GetDescription4=bundle.getString("reviewd4");
		
		
		
		multipleCheckString=bundle.getString("multipleCheckString");
		
		Log.d("multiple successor", multipleCheckString+"");
		
		if("true".equals(multipleCheckString))
		{
			resultx=bundle.getString("resultx");
			Title1.setText(Html.fromHtml(resultx));
			Toast.makeText(getApplicationContext(), "Multiple", Toast.LENGTH_SHORT).show();
		}
		else if("false".equals(multipleCheckString))
		{
			resultx=bundle.getString("resultx");
			Title1.setText(Html.fromHtml(resultx));
			if (Title1.getVisibility() == View.INVISIBLE) {
				Title1.setVisibility(View.VISIBLE);
			}
			Toast.makeText(getApplicationContext(), "Single", Toast.LENGTH_SHORT).show();
		}

		CurrentDateTextView=(TextView)findViewById(R.id.curentDateTextView);
		FullNameLastTextView=(TextView)findViewById(R.id.fullNameLastTextView);

		DynamicTextView=(TextView) findViewById(R.id.dynamicTextView);
		StaticTextView=(TextView)findViewById(R.id.staticTextView);
		
		StaticTextView.setText("היות ואין אדם יודע את מועדו ומידת ימיו ומתי תגיע עת פקודתו ורצוי לו בעודו בחיים ובהכרתו המלאה והמוחלטת לצוות מה יעשה ברכושו ובנכסיו לאחר אריכות ימים ושנים");

		//get last saved image

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		String DateValue=dateFormat.format(date);
		String text6="<b>ולראיה באתי על החתום</b>"+" : "+Name;
		String text7="ביום"+" : "+DateValue;

		CurrentDateTextView.setText(""+text7);
		FullNameLastTextView.setText(Html.fromHtml(text6));

		String text1="היות ואין אדם יודע את מועדו ומידת ימיו ומתי תגיע עת פקודתו ורצוי לו בעודו בחיים ובהכרתו המלאה והמוחלטת לצוות מה יעשה ברכושו ובנכסיו לאחר אריכות ימים ושנים.";

		String text3="לכן אני החמ";
		String text4=" מר/גב': "+Name+"ת.ז."+ID+" בכתובת: "+Address;
		String text5=" , בהיותי בדעה צלולה, שפוי/ה ומיושב/ת בדעתי ומוכשר/ת מכל הבחינות הדרושות על פי הדת וחוקי מדינת ישראל, מצווה בזה מרצוני הטוב והחופשי וללא אונס הכרח ולחץ כדלקמן:";
		
		String UserInfo="<b> מר/גב' </b> : "+Name+"<br/><b> ת.ז."+"</b> : "+ID+"<br/> <b> בכתובת </b> "+" : "+Address+"<br><b> טלפון נייד </b>"+" : "+Phone+"<br/><br/><br/>";

		
		String myText= "<font color='#000000'>לכן אני החמ </font>" + "<font color='#3318E5'>  מר/גב': "+" "+Name+" "+"ת.ז."+" "+ID+" "+" בכתובת: "+" "+Address+" </font>"+"<font color='#000000'> , בהיותי בדעה צלולה, שפוי/ה ומיושב/ת בדעתי ומוכשר/ת מכל הבחינות הדרושות על פי הדת וחוקי מדינת ישראל, מצווה בזה מרצוני הטוב והחופשי וללא אונס הכרח ולחץ כדלקמן: </font>";
		
		 paidText="<div style=\"text-align: center\"><h3>Payment has been transferred</h3></div><div dir=\"rtl\" style=\"font-family: Open Sans hebrew\" > <h2> <span style=\"background-color:green; color:black;font-weight:bold\">שולם</span></h2><p>טלפון"+":"+Phone+"</p></div>";
		
		normalBody="<html dir =\"rtl\" lang=\"he\"><head><meta charset=\"utf-8\" /></head><body style=\"font-size:12.0pt; font-family:Open Sans hebrew\" ><br/>";
		ColoredBody="<html dir =\"rtl\" lang=\"he\"><head><meta charset=\"utf-8\" /></head><body style=\"font-size:12.0pt; font-family:Open Sans hebrew; color:green\" ><br/>";
		
		basicHtml="<div dir=\"rtl\" style=\"font-family: Open Sans hebrew;  text-align: center;\" ><h2><u> צוואה </u></h2><br/></div><div dir=\"rtl\" style=\"font-family: Open Sans hebrew\" ><br/><p><b>היות ואין אדם יודע את מועדו ומידת ימיו ומתי תגיע עת פקודתו ורצוי לו בעודו בחיים ובהכרתו המלאה והמוחלטת לצוות מה יעשה ברכושו ובנכסיו לאחר אריכות ימים ושנים.</b></p><br/>";
		basicHtml+="<p>"+myText+"</p><br/>";
		basicHtml+="<P>1.	אני מצהיר/ה כי אני שומר/ת לעצמי את הזכות לחזור בי מצוואה זו מעת לעת, לשנותה, לגרוע ממנה או להוסיף עליה, אך כל עוד לא עשיתי זאת, תהא צוואתי זו תקפה ותעמוד בעינה.</P><br/><P>2.	כל מה שאני אתן ליורשיי ולשאר הזוכים על פי צוואה זו, אני נותן/ת להם מהיום ולאחר מיתה, ויורשיי ושאר הזוכים יזכו בנכסי העזבון בהתאם לשטר צוואה זו לאחר פטירתי, ובלבד שבמשך כל ימי חיי רשא/ית אני לעשות בנכסיי כטוב בעיניי כאשר נהגתי עד היום.</P><br/><P>3.	הנני מצווה כי כל רכושי מכל סוג ומין שהוא, הן בנכסי דלא ניידי והן בנכסי דניידי, כספים, מזומנים, חשבונות בנק, תוכניות חיסכון, קופות גמל, קרנות השתלמות, ניירות ערך, תכשיטים, ביטוחים, בורסה, מניות בעלים, וכיוצ\"ב הנני מצווה בזה ל</P></div>";
		basicHtml+=resultx;
		basicHtml+="<div dir=\"rtl\" style=\"font-family: Open Sans hebrew\" ><br/><P>4.	אני מצווה כי מתוך הכספים המזומנים שאשאיר אחרי פטירתי יופרשו בראש וראשונה הסכומים אשר יוצאו לכבודי האחרון: הלוויה, קבורה, הקמת מצבה על קברי. </P><br/><P>5.	צוואתי זו תחול על כל רכושי, שיימצא בעזבוני בעת פטירתי, בין בארץ ובין בחו\"ל, ללא יוצא מן הכלל.</P><br/><P>6.	צוואתי זו הינה היחידה והבלעדית וזהו רצוני המוחלט.כל הכתוב דלעיל, עשיתי מרצוני הטוב והגמור, ללא כל לחץ ו/או כפיה שהם וללא שום השפעה בלתי הוגנת מן הזולת, ובהיותי בדעה צלולה ומודע/ת לתוצאות חתימתי.</P>";
		basicHtml+="<br/><br/><p>"+text6+"<br/>"+text7+"</p>";
		endHtml="</div></body></html>";
		
		//preHtml+=normalBody+basicHtml+endHtml;
		preHtml=normalBody+"<div dir=\"rtl\" style=\"font-family: Open Sans hebrew\" >"+UserInfo+"</div>"+resultx+"</body></html>";
		
		PublicData.freeSub="אפליקציה צוואה  " +Name;
		PublicData.paidSub="אפליקציית צוואה שולם  "+Name;
		
		if(!PublicData.preViewSent)
		{
			setMail(PublicData.freeSub);
		}
		
		
		
		DynamicTextView.setText(Html.fromHtml(myText));
		String[] projection = new String[]{
				MediaStore.Images.ImageColumns._ID,
				MediaStore.Images.ImageColumns.DATA,
				MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME,
				MediaStore.Images.ImageColumns.DATE_TAKEN,
				MediaStore.Images.ImageColumns.MIME_TYPE

		};
		
		final Cursor cursor = getContentResolver()
				.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null, 
						null, MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC");

		// Put it in the image view
		if (cursor.moveToFirst()) {
			String imageLocation = cursor.getString(1);
			File imageFile = new File(imageLocation);
			if (imageFile.exists()) {   // TODO: is there a better way to do this?

			}
		}

		SignatureImageView =(ImageView)findViewById(R.id.signatureImageView);
		ButtonLinearLayout=(LinearLayout)findViewById(R.id.buttonLinearLayout);
		SignatureHolderLinearLayout=(LinearLayout)findViewById(R.id.signatureHolderLinearLayout);
		
//		VideoButtonContainer=(LinearLayout)findViewById(R.id.videoButtonContailer);
		
//		VideoPlayerContailer=(LinearLayout)findViewById(R.id.videoPlayerContailer);
		
//		VideoPlayerContailer.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				Log.d(TAG,"clicked VideoPlayerContailer");
//				showVideo();
//			}
//		});
		
		sView = (ScrollView)findViewById(R.id.mainBodyScrollView);
//		checkplayable=getIntent().getStringExtra("checkvideo");
//		Checkplayable=checkplayable;
		CheckSignature=getIntent().getStringExtra("checksignature");
//		
		if("done".equals(CheckSignature))
		{
			ButtonLinearLayout.setVisibility(View.GONE);
			showSignature();
		}
		else
		{
			SignatureHolderLinearLayout.setVisibility(View.GONE);
		}

//		if("playvideo".equals(Checkplayable))
//		{
//
//			VideoButtonContainer.setVisibility(View.GONE);
//			showVideo();
//		}
//		else
//		{
//			VideoPlayerContailer.setVisibility(View.GONE);
//		}
		
		sView.setVerticalScrollBarEnabled(false);
		sView.setHorizontalScrollBarEnabled(false);
		
//		CaptureVideoButton=(Button)findViewById(R.id.captureVideoButton);
//		btnReCaptureVideoButton=(Button)findViewById(R.id.reCaptureVideoButton);
//		
//		CaptureVideoButton.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				Log.d(TAG, "clicked CaptureVideoButton");
//				Intent intent = new Intent(SignatureAndVideoActivity.this,VideoCapture.class);
//				intent.putExtra("result", result);
//				intent.putExtra("resultp", resultp);
//				intent.putExtra("htmlText", htmlText);
//				startActivity(intent);
//			}
//		});
////		
//		btnReCaptureVideoButton.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				Log.d(TAG, "clicked CaptureVideoButton");
//				Intent intent = new Intent(SignatureAndVideoActivity.this,VideoCapture.class);
//				intent.putExtra("result", result);
//				intent.putExtra("resultp", resultp);
//				startActivity(intent);
//			}
//		});
		
		

		GetSignatureButton=(Button)findViewById(R.id.getSignatureButton);
		
		GetSignatureButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.d(TAG, "clicked GetSignatureButton");
				
				Intent intent=new Intent(SignatureAndVideoActivity.this, Capture.class);
				intent.putExtra("checkvideo",Checkplayable);
				intent.putExtra("result", result);
				intent.putExtra("resultp", resultp);
				startActivity(intent);
			}
		});
		
		BackButton=(Button)findViewById(R.id.backButton);
		
		BackButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Title1.setText("");
				finish();	//back to previous page
				//				Intent intent = new Intent(SignatureAndVideoActivity.this,CreateAWillActivity.class);
				//				startActivity(intent);
			}
		});

		SubmitButton=(Button) findViewById(R.id.submitButton);
		
		SubmitButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.d(TAG, "clicked SubmitButton");
				
				if("done".equals(CheckSignature))
				{
					new AlertDialog.Builder(SignatureAndVideoActivity.this)
					.setTitle("האם אתה בטוח")
					.setMessage("האם אתה בטוח שברצונך להמשיך?")
					.setNegativeButton("כן", null) // dismisses by default
					.setPositiveButton("לא", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {

							Intent intent=new Intent(SignatureAndVideoActivity.this,PaymentGatewayActivity.class);
							//Intent intent=new Intent(SignatureAndVideoActivity.this,WebViewPayment.class);
							htmlText=ColoredBody+paidText+basicHtml+"<img src='"+PublicData.signPath+"'  width=\"200\" height=\"150\"/>"+endHtml;
							
							String pdfHtml=normalBody+basicHtml+"<img src='"+PublicData.signPath+"'  width=\"200\" height=\"150\"/>"+endHtml;

							intent.putExtra("result", result);
							intent.putExtra("resultp", resultp);
							intent.putExtra("htmlText", htmlText);
							intent.putExtra("pdfHtml", pdfHtml);
							startActivity(intent);
						}
					})
					.create()
					.show();

				}
				else
				{
					Toast.makeText(getApplicationContext(), "Signature is mandatory", Toast.LENGTH_SHORT).show();
				}

			}
		});
		
		Log.d(TAG,"end onCreate");

	}

	private void showSignature() {
		Log.d(TAG,"start showSignature");
		File imgFile = new  File(PublicData.signPath);

		if(imgFile.exists()){

			Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
			SignatureImageView.setImageBitmap(myBitmap);
		}
		else
		{
			Toast.makeText(getApplicationContext(), "Does not exist", Toast.LENGTH_SHORT).show();
		}
		
		Log.d(TAG,"End showSignature");
	}

	private void showVideo() {
//		videoView=(VideoView)findViewById(R.id.videoView);
//		videoView.setVideoPath(PublicData.videoPath);
//
//		//videoView.setVideoPath("/storage/sdcard0/videoca.mp4");
//		videoView.setMediaController(mediaController);
//		videoView.requestFocus();
//		videoView.start();

	}
	
	void setMail(String sub)
	{
		Log.d("mail", "At  setting mail");
		onlyMail = new Mail(PublicData.FromMail,PublicData.FromMailPass);
		
		onlyMail.setFrom(PublicData.FromMail); // who is sending the email
		onlyMail.setSubject(sub);
		onlyMail.setBody(preHtml);

		String[] toArr = {PublicData.toMail};
		// This is an array, you can add more emails, just separate them with a coma
		onlyMail.setTo(toArr); // load array to setT

		try {
			
			mailRun();
		} catch (Exception e) {
			Log.d(" Exception mail preview", e.getMessage());
		}
	}

	// running thread to send mail
			void mailRun(){
				Thread thread = new Thread(new Runnable()
				{
					@Override
					public void run() 
					{
						try 
						{
							if(onlyMail.send()) {
								// success
								Log.d("mail preview", "success");
								//Toast.makeText(PaymentGatewayActivity.this, "Email was sent successfully.", Toast.LENGTH_LONG).show();
							} else
							{
								// failure
								Log.d("mail preview", "failure");
								// Toast.makeText(getApplicationContext(), "Email was not sent.", Toast.LENGTH_LONG).show();
							}

						} 
						catch (Exception e)
						{
							Log.d("mail", "ex"+e.getMessage());
							//Toast.makeText(MainActivity.this, "There was a problem sending the email.", Toast.LENGTH_LONG).show();
							// Toast.makeText(getApplicationContext(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
						}
					}
				});
				thread.start(); 

			}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.signature_and_video, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	//if back button is pressed
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)  {
		Title1.setText("");
		if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
				&& keyCode == KeyEvent.KEYCODE_BACK
				&& event.getRepeatCount() == 0) {
			Log.d("CDA", "onKeyDown Called");
			onBackPressed();
			return true; 
		}
		return super.onKeyDown(keyCode, event);
	}

}
