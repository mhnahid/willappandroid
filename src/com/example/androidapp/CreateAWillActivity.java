package com.example.androidapp;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.R.integer;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.opengl.Visibility;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.cete.dynamicpdf.Font;
import com.cete.dynamicpdf.TextAlign;
import com.cete.dynamicpdf.merger.od;
import com.cete.dynamicpdf.pageelements.Label;
import com.itextpdf.text.xml.simpleparser.NewLineHandler;

public class CreateAWillActivity extends Activity {

	private String blockCharacterSet = " ";

	String result="";
	String resultp="";
	String resultx="";


	boolean Agree=false;
	boolean toggleSubmenuabletotoggle=true;

	List<EditText> allEditText = new ArrayList<EditText>();
	List<LinearLayout>allView=new ArrayList<LinearLayout>();

	List<String> Datas = new ArrayList<String>();
	List<String> Datas1 = new ArrayList<String>();
	List<String> Datas2 = new ArrayList<String>();
	List<String> Datas3 = new ArrayList<String>();
	List<String> Datas4 = new ArrayList<String>();
	List<String> Datas5 = new ArrayList<String>();
	List<String> Datas6 = new ArrayList<String>();
	List<String> Datas7 = new ArrayList<String>();
	List<String> Datas8 = new ArrayList<String>();
	List<String> Datas9 = new ArrayList<String>();
	List<String> Datas10 = new ArrayList<String>();
	List<String> TopFourColumns = new ArrayList<String>();
	List<String> ExtensibleColumns = new ArrayList<String>();
	List<String> AllDataList = new ArrayList<String>();

	// these map has been created by Nahid, where edittext id will be the key and text will value .
	Map<Integer, String> DataCat1= new HashMap<Integer, String>();  //Real estate
	Map <Integer,String>DataCat2= new HashMap<Integer, String>();  //bank
	Map<Integer, String> DataCat3= new HashMap<Integer, String>(); //savings
	Map<Integer, String> DataCat4= new HashMap<Integer, String>(); // cash
	Map<Integer, String> DataCat5= new HashMap<Integer, String>(); //company share
	Map<Integer, String> DataCat6= new HashMap<Integer, String>(); // security exchange
	Map<Integer, String> DataCat7= new HashMap<Integer, String>(); // jewelery 
	Map<Integer, String> DataCat8= new HashMap<Integer, String>(); // moveable
	Map<Integer, String> DataCat9= new HashMap<Integer, String>(); // share on stock
	Map<Integer, String> DataCat10= new HashMap<Integer, String>(); //another 

	Map< Integer, String> noteMap=new HashMap<Integer, String>();
	
	TextView tvFullName;
	TextView tvIdCard;
	TextView tvAddress;
	TextView tvPhoneNo;

	View mainLayoutTouch;
	//tags

	final int nameItem=1;
	final int branchItem=2;
	final int accountItem=3;
	final int successorItem=4;
	
	String multipleCheckString="true";

	String[][] Section1Array=new String[100][100];

	String TempText="";

	LinearLayout UnUsedLinearLyout1;
	Button UnUsedButton1;
	EditText UnUsedEditText1;
	int UnUsedEditText1id=1040;
	EditText UnUsedEditTextParent1;

	LinearLayout UnUsedLinearLyout2;
	Button UnUsedButton2;
	EditText UnUsedEditText2;
	int UnUsedEditText2id=2040;
	EditText UnUsedEditTextParent2;

	LinearLayout UnUsedLinearLyout3;
	Button UnUsedButton3;
	EditText UnUsedEditText3;
	int UnUsedEditText3id=3040;
	EditText UnUsedEditTextParent3;

	LinearLayout UnUsedLinearLyout4;
	Button UnUsedButton4;
	EditText UnUsedEditText4;
	int UnUsedEditText4id=4040;
	EditText UnUsedEditTextParent4;

	LinearLayout UnUsedLinearLyout5;
	Button UnUsedButton5;
	EditText UnUsedEditText5;
	int UnUsedEditText5id=5040;
	EditText UnUsedEditTextParent5;

	LinearLayout UnUsedLinearLyout6;
	Button UnUsedButton6;
	EditText UnUsedEditText6;
	int UnUsedEditText6id=6040;
	EditText UnUsedEditTextParent6;

	LinearLayout UnUsedLinearLyout7;
	Button UnUsedButton7;
	EditText UnUsedEditText7;
	int UnUsedEditText7id=7040;
	EditText UnUsedEditTextParent7;

	LinearLayout UnUsedLinearLyout8;
	Button UnUsedButton8;
	EditText UnUsedEditText8;
	int UnUsedEditText8id=8040;
	EditText UnUsedEditTextParent8;

	LinearLayout UnUsedLinearLyout9;
	Button UnUsedButton9;
	EditText UnUsedEditText9;
	int UnUsedEditText9id=9040;
	EditText UnUsedEditTextParent9;

	LinearLayout UnUsedLinearLyout10;
	Button UnUsedButton10;
	EditText UnUsedEditText10;
	int UnUsedEditText10id=10040;
	EditText UnUsedEditTextParent10;



	int section1=101;
	int section2=200;
	int section3=300;
	int section4=400;
	int section5=500;
	int section6=600;
	int section7=700;
	int section8=800;
	int section9=900;
	int section10=1000;

	Button BackButton;
	Button SubmitButton;
	EditText TestHebrew;
	int position=0;
	int item=0;
	int lostitemposition=0;


	EditText Comments1EditText;
	EditText Comments2EditText;
	EditText Comments3EditText;
	EditText Comments4EditText;
	EditText Comments9EditText;
	EditText Comments10EditText;

	EditText PersonName;

	ScrollView sView;

	Button ToggleSubMenu1;
	Button ToggleSubMenu2;
	Button ToggleSubMenu3;
	Button ToggleSubMenu4;
	Button ToggleSubMenu5;

	Button ToggleSubMenu6;
	Button ToggleSubMenu7;
	Button ToggleSubMenu8;
	Button ToggleSubMenu9;
	Button ToggleSubMenu10;

	int ToggleSubMenu1CountClick=0;
	int ToggleSubMenu2CountClick=0;
	int ToggleSubMenu3CountClick=0;
	int ToggleSubMenu4CountClick=0;
	int ToggleSubMenu5CountClick=0;
	int ToggleSubMenu6CountClick=0;
	int ToggleSubMenu7CountClick=0;
	int ToggleSubMenu8CountClick=0;
	int ToggleSubMenu9CountClick=0;
	int ToggleSubMenu10CountClick=0;

	LinearLayout Submenu1;
	LinearLayout Submenu2;
	LinearLayout Submenu3;
	LinearLayout Submenu4;
	LinearLayout Submenu5;
	LinearLayout Submenu6;
	LinearLayout Submenu7;
	LinearLayout Submenu8;
	LinearLayout Submenu9;
	LinearLayout Submenu10;
	

	LinearLayout L1;
	LinearLayout L2;
	LinearLayout L3;
	LinearLayout L4;
	LinearLayout L5;
	LinearLayout L6;
	LinearLayout L7;
	LinearLayout L8;
	LinearLayout L9;
	LinearLayout L10;
	LinearLayout audioLayout;

	LinearLayout MultipleSuccessorLayout;
	CheckBox MultipleSuccessorEnableCheckBox;
	boolean MultipleSuccessorCheckBorCheckedOrNot=false;

	Button ExpandL1;
	Button ExpandL2;
	Button ExpandL3;
	Button ExpandL4;
	Button ExpandL5;
	Button ExpandL6;
	Button ExpandL7;
	Button ExpandL8;;
	Button ExpandL9;
	Button ExpandL10;
	
	Button ExpandlAudio;

	CheckBox AgreeCheckBox;
	int numberofunusedbomboclick9=0;
	
	LinearLayout FirstExpansionField;
	LinearLayout SecondExpansionField;
	LinearLayout ThirdExpansionField;
	LinearLayout ForthExpansionField;
	LinearLayout FifthExpansionField;
	LinearLayout SixthExpansionField;
	LinearLayout SeventhExpansionField;
	LinearLayout EighthExpansionField;
	LinearLayout NinthExpansionField;
	LinearLayout TenthExpansionField;

	EditText FullNameEditText;
	EditText IdentityCardEditText;
	EditText AddressEditText;
	EditText PhoneEditText;

	EditText Spbank;
	EditText Spbranch;
	EditText Span;
	EditText Cwatf;
	EditText Cscn;
	EditText Sbank;
	EditText Sbranch;
	EditText Snumber;
	EditText Jdetails;
	EditText Mpdetails;
	EditText Soseabae;
	EditText Odetails;
	EditText Ippa;
	EditText Ipb;
	EditText  Ips;
	EditText Batbn;
	EditText Babn;
	EditText Baan;

	LinearLayout MainLayout;

	TextView AgreeCheckBoxTextView;
	TextView MultipleSuccessorTextView;
	LinearLayout.LayoutParams lp;
	LinearLayout.LayoutParams ml;
	LinearLayout.LayoutParams mr;
	LinearLayout.LayoutParams lpb;
	LinearLayout.LayoutParams et;
	InputMethodManager imm;

	Button play,stop,record,delete;
	private MediaRecorder myAudioRecorder;
	private MediaPlayer corMediaPlayer;
	private String outputFile = null;
	private File audioFile;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_awill);
		// Adding new successor
		
		mainLayoutTouch=(View) findViewById(R.id.scrollView);
		setupUI(findViewById(R.id.scrollView));
		
		
		PublicData.preViewSent=false;
		tvFullName=(TextView) findViewById(R.id.tvFullNameTextBox);
		tvIdCard=(TextView)findViewById(R.id.tvIdentityCardTextBox);
		tvAddress=(TextView)findViewById(R.id.tvAddressTextBox);
		tvPhoneNo=(TextView)findViewById(R.id.tvPhoneTextBox);
		
		String tvFullNameText="<font color='#971d28'> * </font>"+"<font color='#000000'>שם מלא: </font>";
		tvFullName.setText(Html.fromHtml(tvFullNameText));
		
		String tvIdCardText="<font color='#971d28'> * </font>"+"<font color='#000000'>תעודת זהות</font>";
		tvIdCard.setText(Html.fromHtml(tvIdCardText));
		
		String tvAddressText="<font color='#971d28'> * </font>"+"<font color='#000000'>כתובת </font>";
		tvAddress.setText(Html.fromHtml(tvAddressText));
		
		String tvPhoneText="<font color='#971d28'> * </font>"+"<font color='#000000'>טלפון נייד:</font>";
		tvPhoneNo.setText(Html.fromHtml(tvPhoneText));
		
		UnUsedLinearLyout1=(LinearLayout)findViewById(R.id.unUsedLayout1);
//		UnUsedButton1=(Button)findViewById(R.id.unusedButton1);
		UnUsedEditTextParent1=(EditText)findViewById(R.id.unusedEdittextParent1);
		UnUsedEditTextParent1.setTag(UnUsedEditText1id);
		allEditText.add(UnUsedEditTextParent1);

		UnUsedEditTextParent1.setSingleLine(true);


		UnUsedLinearLyout2=(LinearLayout)findViewById(R.id.unUsedLayout2);
//		UnUsedButton2=(Button)findViewById(R.id.unusedButton2);
		UnUsedEditTextParent2=(EditText)findViewById(R.id.unusedEdittextParent2);
		UnUsedEditTextParent2.setTag(UnUsedEditText2id);
		UnUsedEditTextParent2.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				DataCat2.put(2040, s.toString());
				Log.d("Cat 2 ", "s "+s);
				
			}
		});
		
		allEditText.add(UnUsedEditTextParent2);


		UnUsedLinearLyout3=(LinearLayout)findViewById(R.id.unUsedLayout3);
//		UnUsedButton3=(Button)findViewById(R.id.unusedButton3);
		UnUsedEditTextParent3=(EditText)findViewById(R.id.unusedEdittextParent3);
		UnUsedEditTextParent3.setTag(UnUsedEditText3id);
		UnUsedEditTextParent3.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				Log.d(" UnUsedEditTextParent3" ,"Now chsfkjdkf");
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		allEditText.add(UnUsedEditTextParent3);



		UnUsedLinearLyout4=(LinearLayout)findViewById(R.id.unUsedLayout4);
//		UnUsedButton4=(Button)findViewById(R.id.unusedButton4);
		UnUsedEditTextParent4=(EditText)findViewById(R.id.unusedEdittextParent4);
		UnUsedEditTextParent4.setTag(UnUsedEditText4id);
		allEditText.add(UnUsedEditTextParent4);


		UnUsedLinearLyout5=(LinearLayout)findViewById(R.id.unUsedLayout5);
//		UnUsedButton5=(Button)findViewById(R.id.unusedButton5);
		UnUsedEditTextParent5=(EditText)findViewById(R.id.unusedEdittextParent5);
		UnUsedEditTextParent5.setTag(UnUsedEditText5id);
		allEditText.add(UnUsedEditTextParent5);


		UnUsedLinearLyout6=(LinearLayout)findViewById(R.id.unUsedLayout6);
//		UnUsedButton6=(Button)findViewById(R.id.unusedButton6);

		UnUsedEditTextParent6=(EditText)findViewById(R.id.unusedEdittextParent6);
		UnUsedEditTextParent6.setTag(UnUsedEditText6id);
		allEditText.add(UnUsedEditTextParent6);

		UnUsedLinearLyout7=(LinearLayout)findViewById(R.id.unUsedLayout7);
//		UnUsedButton7=(Button)findViewById(R.id.unusedButton7);
		UnUsedEditTextParent7=(EditText)findViewById(R.id.unusedEdittextParent7);
		UnUsedEditTextParent7.setTag(UnUsedEditText7id);
		allEditText.add(UnUsedEditTextParent7);


		UnUsedLinearLyout8=(LinearLayout)findViewById(R.id.unUsedLayout8);
//		UnUsedButton8=(Button)findViewById(R.id.unusedButton8);
		UnUsedEditTextParent8=(EditText)findViewById(R.id.unusedEdittextParent8);
		UnUsedEditTextParent8.setTag(UnUsedEditText8id);
		allEditText.add(UnUsedEditTextParent8);


		UnUsedLinearLyout9=(LinearLayout)findViewById(R.id.unUsedLayout9);
//		UnUsedButton9=(Button)findViewById(R.id.unusedButton9);
		UnUsedEditTextParent9=(EditText)findViewById(R.id.unusedEdittextParent9);
		UnUsedEditTextParent9.setTag(UnUsedEditText9id);
		allEditText.add(UnUsedEditTextParent9);


		UnUsedLinearLyout10=(LinearLayout)findViewById(R.id.unUsedLayout10);
//		UnUsedButton10=(Button)findViewById(R.id.unusedButton10);
		UnUsedEditTextParent10=(EditText)findViewById(R.id.unusedEdittextParent10);
		UnUsedEditTextParent10.setTag(UnUsedEditText10id);
		allEditText.add(UnUsedEditTextParent10);


		lp = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT,82);
		lp.setMargins(-15, 0, 0, 0);

		lpb = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT,18);
		lpb.setMargins(0, 0, 0, -50); 

		ml=new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		ml.setMargins(13,0,0,0);
		mr=new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		mr.setMargins(0, 0, 13, 0);

		MultipleSuccessorTextView=(TextView)findViewById(R.id.multipleSuccessorCheckboxTextView);

		//last button of will create - (form agree button) 
		AgreeCheckBoxTextView=(TextView)findViewById(R.id.agreeCheckBoxTextView);
		AgreeCheckBoxTextView.setTextColor(Color.parseColor("#000000"));

		PersonName=(EditText)findViewById(R.id.personName);
		PersonName.setFocusable(false);
		PersonName.setFocusableInTouchMode(false); // user touches widget on phone with touch screen
		PersonName.setClickable(false);
		PersonName.setBackgroundResource(R.drawable.rectangle_edittext2);

		Comments1EditText=(EditText) findViewById(R.id.comments1EditText);
		Comments2EditText=(EditText)findViewById(R.id.comments2EditText);
		Comments3EditText=(EditText)findViewById(R.id.comments3EditText);
		Comments4EditText=(EditText)findViewById(R.id.comments4EditText);
		Comments9EditText=(EditText)findViewById(R.id.comments9EditText);
		Comments10EditText=(EditText)findViewById(R.id.comments10EditText);

		Comments1EditText.setGravity(Gravity.RIGHT);
		Comments2EditText.setGravity(Gravity.RIGHT);
		Comments3EditText.setGravity(Gravity.RIGHT);
		Comments4EditText.setGravity(Gravity.RIGHT);
		Comments9EditText.setGravity(Gravity.RIGHT);
		Comments10EditText.setGravity(Gravity.RIGHT);
		

		Comments1EditText.setTag(1); //savings plan note
		Comments2EditText.setTag(2); // cash note
		Comments3EditText.setTag(3); //company share note
		Comments4EditText.setTag(4);// security note
		Comments9EditText.setTag(9); // Real state note
		Comments10EditText.setTag(10); // bank note

		//savings Note
		Comments1EditText.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable s) {
				Log.d(" text value after editing savings Note ","Tag :  "+Comments1EditText.getTag()+" "+s.toString());
				noteMap.put((Integer) Comments1EditText.getTag(), Comments1EditText.getText().toString());
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
		});
		//cash note
		Comments2EditText.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable s) {
				Log.d(" text value after editing cash note ","Tag :  "+Comments2EditText.getTag()+" "+s.toString());
				noteMap.put((Integer) Comments2EditText.getTag(), Comments2EditText.getText().toString());
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
		});
		//company share note
		Comments3EditText.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable s) {
				Log.d(" text value after editing company share note ","Tag :  "+Comments3EditText.getTag()+" "+s.toString());
				noteMap.put((Integer) Comments3EditText.getTag(), Comments3EditText.getText().toString());
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
		});
		//security exchange note
		Comments4EditText.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable s) {
				Log.d(" text value after editing security exchange note ","Tag :  "+Comments4EditText.getTag()+" "+s.toString());
				noteMap.put((Integer) Comments4EditText.getTag(), Comments4EditText.getText().toString());
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
		});
		//real estate note
		Comments9EditText.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable s) {
				Log.d(" text value after editing real estate note ","Tag :  "+Comments9EditText.getTag()+" "+s.toString());
				noteMap.put((Integer) Comments9EditText.getTag(), Comments9EditText.getText().toString());
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
		});
		//bank note
		Comments10EditText.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable s) {
				Log.d(" text value after editing bank note ","Tag :  "+Comments10EditText.getTag()+" "+s.toString());
				noteMap.put((Integer) Comments10EditText.getTag(), Comments10EditText.getText().toString());
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
		});



		MultipleSuccessorLayout=(LinearLayout)findViewById(R.id.multipleSuccessorLayout);
		MultipleSuccessorEnableCheckBox=(CheckBox)findViewById(R.id.multipleSuccessorEnableCheckBox);

		// action for mutlitple successor to enable and diable single successor field
		MultipleSuccessorEnableCheckBox.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//is chkIos checked?
				if (((CheckBox) v).isChecked()) {
					//Toast.makeText(getApplicationContext(), "Checked", Toast.LENGTH_SHORT).show();
					if (MultipleSuccessorLayout.getVisibility() == View.VISIBLE) {
						MultipleSuccessorLayout.setVisibility(View.GONE);
						MultipleSuccessorCheckBorCheckedOrNot=true;
						PersonName.setFocusable(true);
						PersonName.setFocusableInTouchMode(true); // user touches widget on phone with touch screen
						PersonName.setClickable(true);
						PersonName.setBackgroundResource(R.drawable.rectangle_edittext);
						MultipleSuccessorTextView.setTextColor(Color.parseColor("#000000"));
						multipleCheckString="false";

					} else {

					}
				}
				else{
					MultipleSuccessorLayout.setVisibility(View.VISIBLE);
					//Toast.makeText(getApplicationContext(), "Not Checked", Toast.LENGTH_SHORT).show();
					MultipleSuccessorCheckBorCheckedOrNot=false;
					//PersonName.setFocusable(true);
					PersonName.setText("");
					PersonName.setFocusable(false);
					PersonName.setFocusableInTouchMode(false); // user touches widget on phone with touch screen
					PersonName.setClickable(false);
					PersonName.setBackgroundResource(R.drawable.rectangle_edittext2);
					MultipleSuccessorTextView.setTextColor(Color.parseColor("#d3d3d3"));
					multipleCheckString="true";

				}
			}
		});

		// end single successor check box

		FullNameEditText=(EditText) findViewById(R.id.fullNameTextBox);
		IdentityCardEditText=(EditText)findViewById(R.id.identityCardTextBox);
		AddressEditText=(EditText)findViewById(R.id.addressTextBox);
		PhoneEditText=(EditText)findViewById(R.id.phoneTextBox);

		FirstExpansionField=(LinearLayout)findViewById(R.id.firstExpansionField);
		SecondExpansionField=(LinearLayout)findViewById(R.id.secondExpansionField);
		ThirdExpansionField=(LinearLayout)findViewById(R.id.thirdExpansionField);
		ForthExpansionField=(LinearLayout)findViewById(R.id.forthExpansionField);
		FifthExpansionField=(LinearLayout)findViewById(R.id.fifthExpansionField);
		SixthExpansionField=(LinearLayout)findViewById(R.id.sixthExpansionField);
		SeventhExpansionField=(LinearLayout)findViewById(R.id.seventhExpansionField);
		EighthExpansionField=(LinearLayout)findViewById(R.id.eighthExpansionField);
		NinthExpansionField=(LinearLayout)findViewById(R.id.ninthExpansionField);
		TenthExpansionField=(LinearLayout)findViewById(R.id.tenthExpansionField);

		FullNameEditText.setGravity(Gravity.RIGHT);
		IdentityCardEditText.setGravity(Gravity.RIGHT);
		AddressEditText.setGravity(Gravity.RIGHT);
		PhoneEditText.setGravity(Gravity.RIGHT);

		AgreeCheckBox=(CheckBox)findViewById(R.id.agreeCheckBox);
		AgreeCheckBox.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//is chkIos checked?
				if (((CheckBox) v).isChecked()) {
					Agree=true;
					//AgreeCheckBoxTextView.setTextColor(Color.parseColor("#000000"));
				}
				else{
					Agree=false;
					//AgreeCheckBoxTextView.setTextColor(Color.parseColor("#d3d3d3"));
				}
			}
		});

		InputFilter filter = new InputFilter()
		{
			@Override
			public CharSequence filter(CharSequence source, int start, int end,
					Spanned dest, int dstart, int dend) {
				for (int i = start; i < end; i++) {
					if (!isHebrew(source.charAt(i))) { 
						return "";
					}
				}
				return null;
			}

			private boolean isHebrew(char c) {
				boolean hebrewDetected = false;
				Pattern p = Pattern.compile("\\p{InHebrew}", Pattern.UNICODE_CASE);
				Matcher m = null;
				m = p.matcher(c+"");
				hebrewDetected = m.matches();
				if(hebrewDetected)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		};
		//TestHebrew.setFilters(new InputFilter[]{filter});

		sView = (ScrollView)findViewById(R.id.scrollView);
		sView.setVerticalScrollBarEnabled(false);
		sView.setHorizontalScrollBarEnabled(false);


		L1=(LinearLayout)findViewById(R.id.l1);
		L2=(LinearLayout)findViewById(R.id.l2);
		L3=(LinearLayout)findViewById(R.id.l3);
		L4=(LinearLayout)findViewById(R.id.l4);
		L5=(LinearLayout)findViewById(R.id.l5);
		L6=(LinearLayout)findViewById(R.id.l6);
		L7=(LinearLayout)findViewById(R.id.l7);
		L8=(LinearLayout)findViewById(R.id.l8);
		L9=(LinearLayout)findViewById(R.id.l9);
		L10=(LinearLayout)findViewById(R.id.l10);
		audioLayout =(LinearLayout)findViewById(R.id.laudio);

		L1.setVisibility(View.GONE);
		L2.setVisibility(View.GONE);
		L3.setVisibility(View.GONE);
		L4.setVisibility(View.GONE);
		L5.setVisibility(View.GONE);
		L6.setVisibility(View.GONE);
		L7.setVisibility(View.GONE);
		L8.setVisibility(View.GONE);
		L9.setVisibility(View.GONE);
		L10.setVisibility(View.GONE);
		audioLayout.setVisibility(View.GONE);


		Spbank =(EditText)findViewById(R.id.spbank);

		Spbranch =(EditText)findViewById(R.id.spbranch);
		Span =(EditText)findViewById(R.id.span);

		Cwatf =(EditText)findViewById(R.id.cwatf);

		Cscn =(EditText)findViewById(R.id.cscn);

		Sbank =(EditText)findViewById(R.id.sbank);
		Sbranch =(EditText)findViewById(R.id.sbranch);
		Snumber =(EditText)findViewById(R.id.snumber);

		Jdetails =(EditText)findViewById(R.id.jdetails);

		Mpdetails =(EditText)findViewById(R.id.mpdetails);

		Soseabae =(EditText)findViewById(R.id.soseabae);

		Odetails =(EditText)findViewById(R.id.odetails);

		Ippa=(EditText) findViewById(R.id.ippa);
		Ipb=(EditText) findViewById(R.id.ipb);// real estate branch
		Ips=(EditText) findViewById(R.id.ips);
		Batbn=(EditText) findViewById(R.id.batbn);
		Babn=(EditText) findViewById(R.id.babn);
		Baan=(EditText) findViewById(R.id.baan);



		// Real estate
		Ippa.setGravity(Gravity.RIGHT);
		//Ippa.setPadding(10, 0, 10, 0);
		Ippa.setTag(Integer.parseInt("1"+PublicData.secOneRowCount+""+nameItem));
		
		Ipb.setGravity(Gravity.RIGHT);
		//Ipb.setPadding(10, 0, 10, 0);
		Ipb.setTag(Integer.parseInt("1"+PublicData.secOneRowCount+""+branchItem));
		
		Ips.setGravity(Gravity.RIGHT);
		//Ips.setPadding(10, 0, 10, 0);
		Ips.setTag(Integer.parseInt("1"+PublicData.secOneRowCount+""+accountItem));

		Ipb.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				Log.d("target Text ", "***** text changing real estate branch");

				DataCat1.put((Integer)Ipb.getTag(), Ipb.getText().toString());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {}
			@Override
			public void afterTextChanged(Editable s) {}
		});
		//real estate name
		Ippa.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				Log.d("target Text ", "***** text changing real estate Name");
				DataCat1.put((Integer)Ippa.getTag(), Ippa.getText().toString());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {}
			@Override
			public void afterTextChanged(Editable s) {}
		});

		Ips.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				Log.d("target Text ", "***** text changing real Account Name");
				DataCat1.put((Integer)Ips.getTag(), Ips.getText().toString());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {}
			@Override
			public void afterTextChanged(Editable s) {}
		});
		// bank

		Batbn.setGravity(Gravity.RIGHT);
		//Batbn.setPadding(10, 0, 10, 0);
		Batbn.setTag(Integer.parseInt("2"+PublicData.secTwoRowCount+""+nameItem));
		
		Babn.setGravity(Gravity.RIGHT);
		//Babn.setPadding(10, 0, 10, 0);
		Babn.setTag(Integer.parseInt("2"+PublicData.secTwoRowCount+""+branchItem));
		
		Baan.setGravity(Gravity.RIGHT);
		//Baan.setPadding(10, 0, 10, 0);
		Baan.setTag(Integer.parseInt("2"+PublicData.secTwoRowCount+""+accountItem));

		Batbn.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				Log.d("target Text ", "***** text Batbn name");
				DataCat2.put((Integer)Batbn.getTag(), Batbn.getText().toString());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {}
			@Override
			public void afterTextChanged(Editable s) {}
		});

		Babn.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				Log.d("target Text ", "***** text Batbn branch");
				DataCat2.put((Integer)Babn.getTag(), Babn.getText().toString());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {}
			@Override
			public void afterTextChanged(Editable s) {}
		});

		Baan.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				Log.d("target Text ", "***** text Batbn");
				DataCat2.put((Integer)Baan.getTag(), Baan.getText().toString());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {}
			@Override
			public void afterTextChanged(Editable s) {}
		});

		// savings plan
		Spbank.setGravity(Gravity.RIGHT);
		//Spbank.setPadding(10, 0, 10, 0);
		Spbank.setTag(Integer.parseInt("3"+PublicData.secThreeRowCount+""+nameItem));

		Spbranch.setGravity(Gravity.RIGHT);
		//Spbranch.setPadding(10, 0, 10, 0);
		Spbranch.setTag(Integer.parseInt("3"+PublicData.secThreeRowCount+""+branchItem));

		Span.setGravity(Gravity.RIGHT);
		//Span.setPadding(10, 0, 10, 0);
		Span.setTag(Integer.parseInt("3"+PublicData.secThreeRowCount+""+accountItem));

		Spbank.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				Log.d("target Text ", "***** text Spbank  "+Spbank.getTag());
				DataCat3.put((Integer)Spbank.getTag(), Spbank.getText().toString());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {}
			@Override
			public void afterTextChanged(Editable s) {}
		});

		Spbranch.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				Log.d("target Text ", "***** text Spbank");
				DataCat3.put((Integer)Spbranch.getTag(), Spbranch.getText().toString());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {}
			@Override
			public void afterTextChanged(Editable s) {}
		});

		Span.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				Log.d("target Text ", "***** text Spbank");
				DataCat3.put((Integer)Span.getTag(), Span.getText().toString());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {}
			@Override
			public void afterTextChanged(Editable s) {}
		});

		//security exchange

		Sbank.setGravity(Gravity.RIGHT);
		//Sbank.setPadding(10, 0, 10, 0);
		Sbank.setTag(Integer.parseInt("6"+PublicData.secSixRowCount+""+nameItem));
		
		Sbranch.setGravity(Gravity.RIGHT);
		//Sbranch.setPadding(10, 0, 10, 0);
		Sbranch.setTag(Integer.parseInt("6"+PublicData.secSixRowCount+""+branchItem));
		
		Snumber.setGravity(Gravity.RIGHT);
		//Snumber.setPadding(10, 0, 10, 0);
		Snumber.setTag(Integer.parseInt("6"+PublicData.secSixRowCount+""+accountItem));


		Sbank.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				Log.d("target Text ", "***** text Sbank");
				DataCat6.put((Integer)Sbank.getTag(), Sbank.getText().toString());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {}
			@Override
			public void afterTextChanged(Editable s) {}
		});

		Sbranch.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				Log.d("target Text ", "***** text Sbank branch");
				DataCat6.put((Integer)Sbranch.getTag(), Sbranch.getText().toString());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {}
			@Override
			public void afterTextChanged(Editable s) {}
		});

		Snumber.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				Log.d("target Text ", "***** text Sbank");
				DataCat6.put((Integer)Snumber.getTag(), Snumber.getText().toString());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {}
			@Override
			public void afterTextChanged(Editable s) {}
		});

		// cash
		Cwatf.setGravity(Gravity.RIGHT);
		//Cwatf.setPadding(10, 0, 10, 0);
		Cwatf.setTag(Integer.parseInt("4"+PublicData.secFourRowCount+""+nameItem));

		Cwatf.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				Log.d("target Text ", "***** text Cwatf");
				DataCat4.put((Integer)Cwatf.getTag(), Cwatf.getText().toString());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {}
			@Override
			public void afterTextChanged(Editable s) {}
		});

		// company share
		Cscn.setGravity(Gravity.RIGHT);
		//Cscn.setPadding(10, 0, 10, 0);
		Cscn.setTag(Integer.parseInt("5"+PublicData.secFiveRowCount+""+nameItem));
		Cscn.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				Log.d("target Text ", "***** text Cscn");
				DataCat5.put((Integer)Cscn.getTag(), Cscn.getText().toString());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {}
			@Override
			public void afterTextChanged(Editable s) {}
		});

		//jewelery
		Jdetails.setGravity(Gravity.RIGHT);
		//Jdetails.setPadding(10, 0, 10, 0);
		Jdetails.setTag(Integer.parseInt("7"+PublicData.secFourRowCount+""+nameItem));
		Jdetails.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				Log.d("target Text ", "***** text Jdetails");
				DataCat7.put((Integer)Jdetails.getTag(), Jdetails.getText().toString());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {}
			@Override
			public void afterTextChanged(Editable s) {}
		});

		// movable property
		Mpdetails.setGravity(Gravity.RIGHT);
		//Mpdetails.setPadding(10, 0, 10, 0);
		Mpdetails.setTag(Integer.parseInt("8"+PublicData.secFourRowCount+""+nameItem));
		Mpdetails.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				Log.d("target Text ", "***** text Mpdetails");
				DataCat8.put((Integer)Mpdetails.getTag(), Mpdetails.getText().toString());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {}
			@Override
			public void afterTextChanged(Editable s) {}
		});

		//
		Soseabae.setGravity(Gravity.RIGHT);
		//Soseabae.setPadding(10, 0, 10, 0);
		Soseabae.setTag(Integer.parseInt("9"+PublicData.secFourRowCount+""+nameItem));
		Soseabae.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				Log.d("target Text ", "***** text Soseabae");
				DataCat9.put((Integer)Soseabae.getTag(), Soseabae.getText().toString());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {}
			@Override
			public void afterTextChanged(Editable s) {}
		});

		// other property
		Odetails.setGravity(Gravity.RIGHT);
		//Odetails.setPadding(10, 0, 10, 0);
		Odetails.setTag(Integer.parseInt("10"+PublicData.secFourRowCount+""+nameItem));
		Odetails.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				Log.d("target Text ", "***** text Odetails");
				DataCat10.put((Integer)Odetails.getTag(), Odetails.getText().toString());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {}
			@Override
			public void afterTextChanged(Editable s) {}
		});


		// Expand start
		ExpandL1=(Button)findViewById(R.id.expandL1);
		ExpandL1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (L1.getVisibility() == View.VISIBLE) {
					L1.setVisibility(View.GONE);
					ExpandL1.setText(" + תכנית חסכון");
				} else {
					L1.setVisibility(View.VISIBLE);
					ExpandL1.setText(" -  תכנית חסכון");
				}
			}
		});

		ExpandL2=(Button)findViewById(R.id.expandL2);
		ExpandL2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (L2.getVisibility() == View.VISIBLE) {
					L2.setVisibility(View.GONE);
					ExpandL2.setText(" + מזומנים");
				} else {
					L2.setVisibility(View.VISIBLE);
					ExpandL2.setText(" -  מזומנים");
				}
			}
		});


		ExpandL3=(Button)findViewById(R.id.expandL3);
		ExpandL3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (L3.getVisibility() == View.VISIBLE) {
					L3.setVisibility(View.GONE);
					ExpandL3.setText(" + מניות בחברה");
				} else {
					L3.setVisibility(View.VISIBLE);
					ExpandL3.setText(" -  מניות בחברה");
				}
			}
		});

		ExpandL4=(Button)findViewById(R.id.expandL4);
		ExpandL4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (L4.getVisibility() == View.VISIBLE) {
					L4.setVisibility(View.GONE);
					ExpandL4.setText(" + ניירות ערך ומטח");
				} else {
					L4.setVisibility(View.VISIBLE);
					ExpandL4.setText(" -  ניירות ערך ומטח");
				}
			}
		});

		ExpandL5=(Button)findViewById(R.id.expandL5);
		ExpandL5.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (L5.getVisibility() == View.VISIBLE) {
					L5.setVisibility(View.GONE);
					ExpandL5.setText(" + תכשיטים");
				} else {
					L5.setVisibility(View.VISIBLE);
					ExpandL5.setText(" -  תכשיטים");
				}
			}
		});

		ExpandL6=(Button)findViewById(R.id.expandL6);
		ExpandL6.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (L6.getVisibility() == View.VISIBLE) {
					L6.setVisibility(View.GONE);
					ExpandL6.setText(" + מטלטלין");
				} else {
					L6.setVisibility(View.VISIBLE);
					ExpandL6.setText(" -  מטלטלין");
				}
			}
		});

		ExpandL7=(Button)findViewById(R.id.expandL7);
		ExpandL7.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (L7.getVisibility() == View.VISIBLE) {
					L7.setVisibility(View.GONE);
					ExpandL7.setText(" + מניות בבורסה");
				} else {
					L7.setVisibility(View.VISIBLE);
					ExpandL7.setText(" -  מניות בבורסה");
				}
			}
		});

		ExpandL8=(Button)findViewById(R.id.expandL8);
		ExpandL8.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (L8.getVisibility() == View.VISIBLE) {
					L8.setVisibility(View.GONE);
					ExpandL8.setText(" + רכוש נוסף");
				} else {
					L8.setVisibility(View.VISIBLE);
					ExpandL8.setText(" -  רכוש נוסף");
				}
			}
		});
		ExpandL9=(Button)findViewById(R.id.expandL9);
		ExpandL9.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (L9.getVisibility() == View.VISIBLE) {
					L9.setVisibility(View.GONE);
					ExpandL9.setText(" + מקרקעין (נכסי דלא ניידי ונכסי דניידי)");
				} else {
					L9.setVisibility(View.VISIBLE);
					ExpandL9.setText(" - מקרקעין (נכסי דלא ניידי ונכסי דניידי)");
				}
			}
		});
		ExpandL10=(Button)findViewById(R.id.expandL10);
		ExpandL10.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (L10.getVisibility() == View.VISIBLE) {
					L10.setVisibility(View.GONE);
					ExpandL10.setText(" + חשבונות בנק");
				} else {
					L10.setVisibility(View.VISIBLE);
					ExpandL10.setText(" - חשבונות בנק");
				}
			}
		});
		
		ExpandlAudio=(Button)findViewById(R.id.recordaudio);
		ExpandlAudio.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (audioLayout.getVisibility() == View.VISIBLE) {
					audioLayout.setVisibility(View.GONE);
					ExpandlAudio.setText(" + קול הרשומה");
				} else {
					audioLayout.setVisibility(View.VISIBLE);
					ExpandlAudio.setText(" - קול הרשומה");
				}
				
			}
		});

		// Expand end
		
		final Drawable delback = getResources().getDrawable(R.drawable.rectangle_edittextred);

		ToggleSubMenu1=(Button)findViewById(R.id.toggleSubMenu1);
		//LinearLayout mainlayout = (LinearLayout) inflater.inflate(R.layout.dyna, null);

		// showing data to expanded table

		final int noteItem=5;



		ToggleSubMenu1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {


				Log.d("tag value for row count", " rowCout : "+PublicData.secThreeRowCount);
				int sectionNo=3;
				int currentRow=PublicData.secThreeRowCount;

				currentRow++;
				PublicData.secThreeRowCount=currentRow;

				int nameTag=Integer.parseInt(""+sectionNo+""+currentRow+""+nameItem);
				int branchTag=Integer.parseInt(""+sectionNo+""+currentRow+""+branchItem);
				int accountTag=Integer.parseInt(""+sectionNo+""+currentRow+""+accountItem);

				int noteTag=Integer.parseInt(""+sectionNo+""+noteItem+""+currentRow);
				int textViewTag=Integer.parseInt(""+sectionNo+"000"+currentRow);

				Log.d("tag value "," nametag "+nameTag+" , branchTag "+branchTag+" , account tag "+accountTag);

				toggleSubmenuabletotoggle=true;


				if(toggleSubmenuabletotoggle)
				{
					ToggleSubMenu1CountClick++;

					LinearLayout parent = new LinearLayout(getApplicationContext());
					parent.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
					parent.setOrientation(LinearLayout.VERTICAL);
					parent.setTag(textViewTag);
					allView.add(parent);

					LinearLayout FirstBlockLayout =new LinearLayout(getApplicationContext());
					FirstBlockLayout.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT,10));
					FirstBlockLayout.setOrientation(LinearLayout.HORIZONTAL);
					FirstBlockLayout.setGravity(Gravity.RIGHT);
					FirstBlockLayout.setPadding(0, 0, 0, 25);
					
					
					String name=Spbank.getText().toString();
					String branch=Spbranch.getText().toString();
					String account=Span.getText().toString();
					String successor=UnUsedEditTextParent3.getText().toString();
					
					
					String text="פירוט"+" : "+name+"<br/>"+"סניף"+":"+branch+"<br/>"+"מספר חשבון"+" : "+account+"<br/>"+"שם היורש\\יורשים"+" : "+successor;

					TextView tv1 = new TextView(getApplicationContext());
					tv1.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,(float) 9.2));
					tv1.setText(Html.fromHtml(text));
					tv1.setGravity(Gravity.RIGHT);
					tv1.setTextColor(Color.parseColor("#000000"));
					tv1.setPadding(0, 0, 0, 40);
					tv1.setTypeface(null, Typeface.BOLD);
					
					
					Button deleteView=new Button(getApplicationContext());
					
					deleteView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,(float)0.8));
					deleteView.setText("מחק");
					deleteView.setTag(textViewTag);
					deleteView.setGravity(Gravity.CENTER);
					deleteView.setTextColor(Color.WHITE);
					deleteView.setBackground(delback);
					deleteView.setTypeface(null, Typeface.BOLD);
					deleteView.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Log.d("Button tag", v.getTag()+" is buttons");
							int buttonTag=(Integer) v.getTag();
							for(int i=0;i<allView.size();i++)
							{
								LinearLayout lay=allView.get(i);
								int tag=(Integer) lay.getTag();
								Log.d("delete text view", ""+tag);
								if(tag==buttonTag)
								{
									lay.setVisibility(LinearLayout.GONE);
									DataCat3.put(tag, "");
									
								}
							}
							
						}
					});
					

					section1++;
					FirstBlockLayout.addView(deleteView);
					FirstBlockLayout.addView(tv1);
					
					parent.addView(FirstBlockLayout);

					parent.setPadding(0, 25, 0, 0);

					FirstExpansionField.addView(parent);
					
					UnUsedEditText3id=3040;
					Spbank.setText("");
					Spbranch.setText("");
					Span.setText("");
					UnUsedEditTextParent3.setText("");
					
					DataCat3.put((Integer)Spbank.getTag(), Sbank.getText().toString());
					DataCat3.put((Integer)Spbranch.getTag(), Spbranch.getText().toString());
					DataCat3.put((Integer)Span.getTag(), Span.getText().toString());
					DataCat3.put(3040, "");
					DataCat3.put(textViewTag, text);
				}
			}
		});
		ToggleSubMenu2=(Button)findViewById(R.id.toggleSubMenu2);
		ToggleSubMenu2.setOnClickListener(new OnClickListener() {
			int successorTag=0;

			@Override
			public void onClick(View v) {
				int sectionNo=4;
				int currentRow=PublicData.secFourRowCount;

				currentRow++;
				PublicData.secFourRowCount=currentRow;

				int nameTag=Integer.parseInt(""+sectionNo+""+currentRow+""+nameItem);
				successorTag=Integer.parseInt(""+sectionNo+""+currentRow+""+successorItem+"1");
				int textViewTag=Integer.parseInt(""+sectionNo+"000"+currentRow);
				toggleSubmenuabletotoggle=true;

				if(toggleSubmenuabletotoggle)
				{
					Log.d("logcat", "at adding new");
					ToggleSubMenu2CountClick++;
					LinearLayout parent2 = new LinearLayout(getApplicationContext());
					parent2.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
					parent2.setOrientation(LinearLayout.VERTICAL);
					parent2.setTag(textViewTag);
					allView.add(parent2);

					LinearLayout L2B1 =new LinearLayout(getApplicationContext());
					L2B1.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT,10));
					L2B1.setOrientation(LinearLayout.HORIZONTAL);
					L2B1.setGravity(Gravity.RIGHT);
					L2B1.setPadding(0, 0, 0, 25);
					
					
					String details=Cwatf.getText().toString();
					String successor=UnUsedEditTextParent4.getText().toString();
					
					String text="פירוט"+" : "+details+"<br/>"+"שם היורש\\יורשים"+" : "+successor;

					TextView L2B1tv1 = new TextView(getApplicationContext());
					L2B1tv1.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,(float) 9.2));
					L2B1tv1.setText(Html.fromHtml(text));
					L2B1tv1.setGravity(Gravity.RIGHT);
					L2B1tv1.setTextColor(Color.parseColor("#000000"));
					L2B1tv1.setTypeface(null, Typeface.BOLD);
					
					Button deleteView=new Button(getApplicationContext());
					
					deleteView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,(float)0.8));
					deleteView.setText("מחק");
					deleteView.setTag(textViewTag);
					deleteView.setGravity(Gravity.CENTER);
					deleteView.setTextColor(Color.WHITE);
					deleteView.setBackground(delback);
					deleteView.setTypeface(null, Typeface.BOLD);
					deleteView.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Log.d("Button tag", v.getTag()+" is buttons");
							int buttonTag=(Integer) v.getTag();
							for(int i=0;i<allView.size();i++)
							{
								LinearLayout lay=allView.get(i);
								int tag=(Integer) lay.getTag();
								Log.d("delete text view", ""+tag);
								if(tag==buttonTag)
								{
									lay.setVisibility(LinearLayout.GONE);
									DataCat4.put(tag, "");
									
								}
							}
							
						}
					});
					
					section2++;
					L2B1.addView(deleteView);
					L2B1.addView(L2B1tv1);
					parent2.addView(L2B1);
					
					parent2.setPadding(0, 25, 0, 25);

					SecondExpansionField.addView(parent2);
					
					UnUsedEditText4id=4040;
					Cwatf.setText("");
					UnUsedEditTextParent4.setText("");
					DataCat4.put((Integer) Cwatf.getTag(), Cwatf.getText().toString());
					DataCat4.put(4040, "");
					DataCat4.put(textViewTag, text);
				}
			}
		});
		ToggleSubMenu3=(Button)findViewById(R.id.toggleSubMenu3);
		ToggleSubMenu3.setOnClickListener(new OnClickListener() {
			int successorTag=0;

			@Override
			public void onClick(View v) {
				int sectionNo=5;
				int currentRow=PublicData.secFiveRowCount;

				currentRow++;
				PublicData.secFiveRowCount=currentRow;

				int nameTag=Integer.parseInt(""+sectionNo+""+currentRow+""+nameItem);
				successorTag=Integer.parseInt(""+sectionNo+""+currentRow+""+successorItem+"1");
				
				int textViewTag=Integer.parseInt(""+sectionNo+"000"+currentRow);

				toggleSubmenuabletotoggle=true;
				if(toggleSubmenuabletotoggle)
				{
					ToggleSubMenu3CountClick++;
					LinearLayout parent2 = new LinearLayout(getApplicationContext());
					parent2.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
					parent2.setOrientation(LinearLayout.VERTICAL);
					parent2.setTag(textViewTag);
					allView.add(parent2);

					LinearLayout L2B1 =new LinearLayout(getApplicationContext());
					L2B1.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT,10));
					L2B1.setOrientation(LinearLayout.HORIZONTAL);
					L2B1.setGravity(Gravity.RIGHT);
					L2B1.setPadding(0, 0, 0, 25);
					
					String details=Cscn.getText().toString();
					String successor=UnUsedEditTextParent5.getText().toString();
					
					String text="פירוט"+" : "+details+"<br/>"+"שם היורש\\יורשים"+" : "+successor;
					
					

					TextView L2B1tv1 = new TextView(getApplicationContext());
					L2B1tv1.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,(float) 9.2));
					L2B1tv1.setText(Html.fromHtml(text));
					L2B1tv1.setGravity(Gravity.RIGHT);
					L2B1tv1.setTextColor(Color.parseColor("#000000"));
					L2B1tv1.setTypeface(null, Typeface.BOLD);
					
					Button deleteView=new Button(getApplicationContext());
					
					deleteView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,(float)0.8));
					deleteView.setText("מחק");
					deleteView.setTag(textViewTag);
					deleteView.setGravity(Gravity.CENTER);
					deleteView.setTextColor(Color.WHITE);
					deleteView.setBackground(delback);
					deleteView.setTypeface(null, Typeface.BOLD);
					deleteView.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Log.d("Button tag", v.getTag()+" is buttons");
							int buttonTag=(Integer) v.getTag();
							for(int i=0;i<allView.size();i++)
							{
								LinearLayout lay=allView.get(i);
								int tag=(Integer) lay.getTag();
								Log.d("delete text view", ""+tag);
								if(tag==buttonTag)
								{
									lay.setVisibility(LinearLayout.GONE);
									DataCat5.put(tag, "");
									
								}
							}
							
						}
					});
					
					section3++;
					L2B1.addView(deleteView);
					L2B1.addView(L2B1tv1);					
					parent2.addView(L2B1);					
					parent2.setPadding(0, 25, 0, 25);
					ThirdExpansionField.addView(parent2);
					
					
					UnUsedEditText5id=5040;
					Cscn.setText("");
					UnUsedEditTextParent5.setText("");
					DataCat5.put((Integer) Cscn.getTag(), Cscn.getText().toString());
					DataCat5.put(5040, "");
					DataCat5.put(textViewTag, text);
					
				}

				getEditTextValue();
			}
		});
		ToggleSubMenu4=(Button)findViewById(R.id.toggleSubMenu4);
		ToggleSubMenu4.setOnClickListener(new OnClickListener() {
			int successorTag=0;
			@Override
			public void onClick(View v) {
				int sectionNo=6;
				int currentRow=PublicData.secSixRowCount;

				currentRow++;
				PublicData.secSixRowCount=currentRow;

				int nameTag=Integer.parseInt(""+sectionNo+""+currentRow+""+nameItem);
				int branchTag=Integer.parseInt(""+sectionNo+""+currentRow+""+branchItem);
				int accountTag=Integer.parseInt(""+sectionNo+""+currentRow+""+accountItem);
				successorTag=Integer.parseInt(""+sectionNo+""+currentRow+""+successorItem+"1");

				int textViewTag=Integer.parseInt(""+sectionNo+"000"+currentRow);

				toggleSubmenuabletotoggle=true;

				if(toggleSubmenuabletotoggle)
				{
					ToggleSubMenu4CountClick++;
					LinearLayout parent = new LinearLayout(getApplicationContext());
					parent.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
					parent.setOrientation(LinearLayout.VERTICAL);
					parent.setTag(textViewTag);
					allView.add(parent);

					LinearLayout FirstBlockLayout =new LinearLayout(getApplicationContext());
					FirstBlockLayout.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT,10));
					FirstBlockLayout.setOrientation(LinearLayout.HORIZONTAL);
					FirstBlockLayout.setGravity(Gravity.RIGHT);
					FirstBlockLayout.setPadding(0, 0, 0, 25);
					
					String name=Sbank.getText().toString();
					String branch=Sbranch.getText().toString();
					String account=Snumber.getText().toString();
					String successor=UnUsedEditTextParent6.getText().toString();
					
					
					String text="פירוט"+" : "+name+"<br/>"+"סניף"+":"+branch+"<br/>"+"מספר חשבון"+" : "+account+"<br/>"+"שם היורש\\יורשים"+" : "+successor;
					
					TextView tv1 = new TextView(getApplicationContext());
					tv1.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,(float) 9.2));
					tv1.setText(Html.fromHtml(text));
					tv1.setGravity(Gravity.RIGHT);
					tv1.setTextColor(Color.parseColor("#000000"));
					tv1.setPadding(0, 0, 0, 40);
					tv1.setTypeface(null, Typeface.BOLD);
					
					Button deleteView=new Button(getApplicationContext());
					
					deleteView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,(float)0.8));
					deleteView.setText("מחק");
					deleteView.setTag(textViewTag);
					deleteView.setGravity(Gravity.CENTER);
					deleteView.setTextColor(Color.WHITE);
					deleteView.setBackground(delback);
					deleteView.setTypeface(null, Typeface.BOLD);
					deleteView.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Log.d("Button tag", v.getTag()+" is buttons");
							int buttonTag=(Integer) v.getTag();
							for(int i=0;i<allView.size();i++)
							{
								LinearLayout lay=allView.get(i);
								int tag=(Integer) lay.getTag();
								Log.d("delete text view", ""+tag);
								if(tag==buttonTag)
								{
									lay.setVisibility(LinearLayout.GONE);
									DataCat6.put(tag, "");
									
								}
							}
							
						}
					});

					section4++;
					FirstBlockLayout.addView(deleteView);
					FirstBlockLayout.addView(tv1);
					
					parent.addView(FirstBlockLayout);
					
					parent.setPadding(0, 25, 0, 25);
					ForthExpansionField.addView(parent);
					
					
					UnUsedEditText6id=6040;
					Sbank.setText("");
					Sbranch.setText("");
					Snumber.setText("");
					UnUsedEditTextParent6.setText("");
					DataCat6.put((Integer) Sbank.getTag(), Sbank.getText().toString());
					DataCat6.put((Integer) Sbranch.getTag(), Sbranch.getText().toString());
					DataCat6.put((Integer) Snumber.getTag(), Snumber.getText().toString());
					DataCat6.put(6040, "");
					DataCat6.put(textViewTag, text);
				}
			}
		});
		ToggleSubMenu5=(Button)findViewById(R.id.toggleSubMenu5);
		ToggleSubMenu5.setOnClickListener(new OnClickListener() {
			int successorTag=0;
			@Override
			public void onClick(View v) {
				int sectionNo=7;
				int currentRow=PublicData.secSevenRowCount;

				currentRow++;
				PublicData.secSevenRowCount=currentRow;

				int nameTag=Integer.parseInt(""+sectionNo+""+currentRow+""+nameItem);
				successorTag=Integer.parseInt(""+sectionNo+""+currentRow+""+successorItem+"1");
				
				int textViewTag=Integer.parseInt(""+sectionNo+"000"+currentRow);

				toggleSubmenuabletotoggle=true;
				if(toggleSubmenuabletotoggle)
				{
					ToggleSubMenu5CountClick++;
					LinearLayout parent2 = new LinearLayout(getApplicationContext());
					parent2.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
					parent2.setOrientation(LinearLayout.VERTICAL);
					parent2.setTag(textViewTag);
					allView.add(parent2);

					LinearLayout L2B1 =new LinearLayout(getApplicationContext());
					L2B1.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT,10));
					L2B1.setOrientation(LinearLayout.HORIZONTAL);
					L2B1.setGravity(Gravity.RIGHT);
					L2B1.setPadding(0, 0, 0, 25);
					
					String details=Jdetails.getText().toString();
					String successor=UnUsedEditTextParent7.getText().toString();
					
					
					String text="פירוט"+" : "+details+"<br/>"+"שם היורש\\יורשים"+" : "+successor;

					TextView L2B1tv1 = new TextView(getApplicationContext());
					L2B1tv1.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,(float) 9.2));
					L2B1tv1.setText(Html.fromHtml(text));
					L2B1tv1.setGravity(Gravity.RIGHT);
					L2B1tv1.setTextColor(Color.parseColor("#000000"));
					L2B1tv1.setTypeface(null, Typeface.BOLD);
					
					Button deleteView=new Button(getApplicationContext());
					
					deleteView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,(float)0.8));
					deleteView.setText("מחק");
					deleteView.setTag(textViewTag);
					deleteView.setGravity(Gravity.CENTER);
					deleteView.setTextColor(Color.WHITE);
					deleteView.setBackground(delback);
					deleteView.setTypeface(null, Typeface.BOLD);
					deleteView.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Log.d("Button tag", v.getTag()+" is buttons");
							int buttonTag=(Integer) v.getTag();
							for(int i=0;i<allView.size();i++)
							{
								LinearLayout lay=allView.get(i);
								int tag=(Integer) lay.getTag();
								Log.d("delete text view", ""+tag);
								if(tag==buttonTag)
								{
									lay.setVisibility(LinearLayout.GONE);
									DataCat7.put(tag, "");
									
								}
							}
							
						}
					});


					section5++;
					L2B1.addView(deleteView);
					L2B1.addView(L2B1tv1);
					
					parent2.addView(L2B1);
					
					parent2.setPadding(0, 25, 0, 25);
					FifthExpansionField.addView(parent2);
					
					
					UnUsedEditText7id=7040;
					Jdetails.setText("");
					UnUsedEditTextParent7.setText("");
					DataCat7.put((Integer) Jdetails.getTag(), Jdetails.getText().toString());
					DataCat7.put(7040, "");
					DataCat7.put(textViewTag, text);
				}
			}
		});

		ToggleSubMenu6=(Button)findViewById(R.id.toggleSubMenu6);
		ToggleSubMenu6.setOnClickListener(new OnClickListener() {
			int successorTag=0;
			@Override
			public void onClick(View v) {
				int sectionNo=8;
				int currentRow=PublicData.secEightRowCount;

				currentRow++;
				PublicData.secEightRowCount=currentRow;

				int nameTag=Integer.parseInt(""+sectionNo+""+currentRow+""+nameItem);
				successorTag=Integer.parseInt(""+sectionNo+""+currentRow+""+successorItem+"1");
				
				int textViewTag=Integer.parseInt(""+sectionNo+"000"+currentRow);

				toggleSubmenuabletotoggle=true;

				if(toggleSubmenuabletotoggle)
				{
					ToggleSubMenu6CountClick++;
					LinearLayout parent2 = new LinearLayout(getApplicationContext());
					parent2.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
					parent2.setOrientation(LinearLayout.VERTICAL);
					parent2.setTag(textViewTag);
					allView.add(parent2);

					LinearLayout L2B1 =new LinearLayout(getApplicationContext());
					L2B1.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT,10));
					L2B1.setOrientation(LinearLayout.HORIZONTAL);
					L2B1.setGravity(Gravity.RIGHT);
					L2B1.setPadding(0, 0, 0, 25);
					
					String details=Mpdetails.getText().toString();
					String successor=UnUsedEditTextParent8.getText().toString();
					
					
					
					String text="פירוט"+" : "+details+"<br/>"+"שם היורש\\יורשים"+" : "+successor;

					TextView L2B1tv1 = new TextView(getApplicationContext());
					L2B1tv1.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,(float) 9.2));
					L2B1tv1.setText(Html.fromHtml(text));
					L2B1tv1.setGravity(Gravity.RIGHT);
					L2B1tv1.setTextColor(Color.parseColor("#000000"));
					L2B1tv1.setTypeface(null, Typeface.BOLD);
					
					Button deleteView=new Button(getApplicationContext());
					
					deleteView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,(float)0.8));
					deleteView.setText("מחק");
					deleteView.setTag(textViewTag);
					deleteView.setGravity(Gravity.CENTER);
					deleteView.setTextColor(Color.WHITE);
					deleteView.setBackground(delback);
					deleteView.setTypeface(null, Typeface.BOLD);
					deleteView.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Log.d("Button tag", v.getTag()+" is buttons");
							int buttonTag=(Integer) v.getTag();
							for(int i=0;i<allView.size();i++)
							{
								LinearLayout lay=allView.get(i);
								int tag=(Integer) lay.getTag();
								Log.d("delete text view", ""+tag);
								if(tag==buttonTag)
								{
									lay.setVisibility(LinearLayout.GONE);
									DataCat8.put(tag, "");
									
								}
							}
							
						}
					});


					section6++;
				
					L2B1.addView(deleteView);
					L2B1.addView(L2B1tv1);
					parent2.addView(L2B1);
					parent2.setPadding(0, 25, 0, 25);
					SixthExpansionField.addView(parent2);
					
					
					UnUsedEditText8id=8040;
					Mpdetails.setText("");
					UnUsedEditTextParent8.setText("");
					DataCat8.put((Integer) Mpdetails.getTag(), Mpdetails.getText().toString());
					DataCat8.put(8040, "");
					DataCat8.put(textViewTag, text);
				}
			}
		});
		ToggleSubMenu7=(Button)findViewById(R.id.toggleSubMenu7);
		ToggleSubMenu7.setOnClickListener(new OnClickListener() {
			int successorTag=0;
			@Override
			public void onClick(View v) {
				int sectionNo=9;
				int currentRow=PublicData.secNineRowCount;

				currentRow++;
				PublicData.secNineRowCount=currentRow;

				int nameTag=Integer.parseInt(""+sectionNo+""+currentRow+""+nameItem);
				successorTag=Integer.parseInt(""+sectionNo+""+currentRow+""+successorItem+"1");
				
				int textViewTag=Integer.parseInt(""+sectionNo+"000"+currentRow);

				toggleSubmenuabletotoggle=true;

				if(toggleSubmenuabletotoggle)
				{
					ToggleSubMenu7CountClick++;
					LinearLayout parent2 = new LinearLayout(getApplicationContext());
					parent2.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
					parent2.setOrientation(LinearLayout.VERTICAL);
					parent2.setTag(textViewTag);
					allView.add(parent2);

					LinearLayout L2B1 =new LinearLayout(getApplicationContext());
					L2B1.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT,10));
					L2B1.setOrientation(LinearLayout.HORIZONTAL);
					L2B1.setGravity(Gravity.RIGHT);
					L2B1.setPadding(0, 0, 0, 25);
					
					String details=Soseabae.getText().toString();
					String successor=UnUsedEditTextParent9.getText().toString();
					
					
					String text="פירוט"+" : "+details+"<br/>"+"שם היורש\\יורשים"+" : "+successor;

					TextView L2B1tv1 = new TextView(getApplicationContext());
					L2B1tv1.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,(float) 9.2));
					L2B1tv1.setText(Html.fromHtml(text));
					L2B1tv1.setGravity(Gravity.RIGHT);
					L2B1tv1.setTextColor(Color.parseColor("#000000"));
					L2B1tv1.setTypeface(null, Typeface.BOLD);
					
					Button deleteView=new Button(getApplicationContext());
					
					deleteView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,(float)0.8));
					deleteView.setText("מחק");
					deleteView.setTag(textViewTag);
					deleteView.setGravity(Gravity.CENTER);
					deleteView.setTextColor(Color.WHITE);
					deleteView.setBackground(delback);
					deleteView.setTypeface(null, Typeface.BOLD);
					deleteView.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Log.d("Button tag", v.getTag()+" is buttons");
							int buttonTag=(Integer) v.getTag();
							for(int i=0;i<allView.size();i++)
							{
								LinearLayout lay=allView.get(i);
								int tag=(Integer) lay.getTag();
								Log.d("delete text view", ""+tag);
								if(tag==buttonTag)
								{
									lay.setVisibility(LinearLayout.GONE);
									DataCat9.put(tag, "");
									
								}
							}
							
						}
					});


					section7++;

					L2B1.addView(deleteView);
					L2B1.addView(L2B1tv1);
					
					parent2.addView(L2B1);
					
					parent2.setPadding(0, 25, 0, 25);
					SeventhExpansionField.addView(parent2);
					
					UnUsedEditText9id=9040;
					
					Soseabae.setText("");
					UnUsedEditTextParent9.setText("");
					DataCat9.put((Integer) Soseabae.getTag(), Soseabae.getText().toString());
					DataCat9.put(9040, "");
					DataCat9.put(textViewTag, text);
				}
			}
		});
		ToggleSubMenu8=(Button)findViewById(R.id.toggleSubMenu8);
		ToggleSubMenu8.setOnClickListener(new OnClickListener() {
			int successorTag=0;
			@Override
			public void onClick(View v) {

				int sectionNo=10;
				int currentRow=PublicData.secTenRowCount;

				currentRow++;
				PublicData.secTenRowCount=currentRow;

				int nameTag=Integer.parseInt(""+sectionNo+""+currentRow+""+nameItem);
				successorTag=Integer.parseInt(""+sectionNo+""+currentRow+""+successorItem+"1");
				
				int textViewTag=Integer.parseInt(""+sectionNo+"000"+currentRow);

				toggleSubmenuabletotoggle=true;

				if(toggleSubmenuabletotoggle)
				{
					ToggleSubMenu8CountClick++;
					LinearLayout parent2 = new LinearLayout(getApplicationContext());
					parent2.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
					parent2.setOrientation(LinearLayout.VERTICAL);
					parent2.setTag(textViewTag);
					allView.add(parent2);

					LinearLayout L2B1 =new LinearLayout(getApplicationContext());
					L2B1.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT,10));
					L2B1.setOrientation(LinearLayout.HORIZONTAL);
					L2B1.setGravity(Gravity.RIGHT);
					L2B1.setPadding(0, 0, 0, 25);
					
					String details=Odetails.getText().toString();
					String successor=UnUsedEditTextParent10.getText().toString();
					
					
					
					String text="פירוט"+" : "+details+"<br/>"+"שם היורש\\יורשים"+" : "+successor;

					TextView L2B1tv1 = new TextView(getApplicationContext());
					L2B1tv1.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,(float) 9.2));
					L2B1tv1.setText(Html.fromHtml(text));
					L2B1tv1.setGravity(Gravity.RIGHT);
					L2B1tv1.setTextColor(Color.parseColor("#000000"));
					L2B1tv1.setTypeface(null, Typeface.BOLD);
					
					Button deleteView=new Button(getApplicationContext());
					
					deleteView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,(float)0.8));
					deleteView.setText("מחק");
					deleteView.setTag(textViewTag);
					deleteView.setGravity(Gravity.CENTER);
					deleteView.setTextColor(Color.WHITE);
					deleteView.setBackground(delback);
					deleteView.setTypeface(null, Typeface.BOLD);
					deleteView.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Log.d("Button tag", v.getTag()+" is buttons");
							int buttonTag=(Integer) v.getTag();
							for(int i=0;i<allView.size();i++)
							{
								LinearLayout lay=allView.get(i);
								int tag=(Integer) lay.getTag();
								Log.d("delete text view", ""+tag);
								if(tag==buttonTag)
								{
									lay.setVisibility(LinearLayout.GONE);
									DataCat10.put(tag, "");
									
								}
							}
							
						}
					});


					section8++;
					L2B1.addView(deleteView);
					L2B1.addView(L2B1tv1);
					
					parent2.addView(L2B1);
					parent2.setPadding(0, 25, 0, 25);
					EighthExpansionField.addView(parent2,0);
					
					UnUsedEditText10id=10040;
					Odetails.setText("");
					UnUsedEditTextParent10.setText("");
					DataCat10.put((Integer) Odetails.getTag(), Odetails.getText().toString());
					DataCat10.put(10040, "");
					DataCat10.put(textViewTag, text);
				}
			}
		});


//#####################################################################
		
		  play=(Button)findViewById(R.id.buttonPlay);
	      stop=(Button)findViewById(R.id.buttonStop);
	      record=(Button)findViewById(R.id.buttonRecord);
	      delete=(Button)findViewById(R.id.buttonDelete);
	      stop.setEnabled(false);
	      play.setEnabled(false);
	      outputFile = PublicData.audioPath;
	      
	       audioFile= new File(PublicData.audioPath);
	      if(audioFile.exists())
	      {
	    	  delete.setEnabled(true);
	      }else {
			delete.setEnabled(false);
		}
	      
	      myAudioRecorder=new MediaRecorder();
	      myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
	      myAudioRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
	      myAudioRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
	      myAudioRecorder.setOutputFile(outputFile);
	      
	      record.setOnClickListener(new View.OnClickListener() {
	         @Override
	         public void onClick(View v) {
	            try {
	               myAudioRecorder.prepare();
	               myAudioRecorder.start();
	            }
	            
	            catch (IllegalStateException e) {
	               // TODO Auto-generated catch block
	               e.printStackTrace();
	            }
	            
	            catch (IOException e) {
	               // TODO Auto-generated catch block
	               e.printStackTrace();
	            }
	            
	            record.setEnabled(false);
	            stop.setEnabled(true);
	            
	            Toast.makeText(getApplicationContext(), "Recording started", Toast.LENGTH_LONG).show();
	         }
	      });
	      
	      stop.setOnClickListener(new View.OnClickListener() {
	         @Override
	         public void onClick(View v) {
	            myAudioRecorder.stop();
	            myAudioRecorder.release();
	            myAudioRecorder  = null;
	            
	            stop.setEnabled(false);
	            record.setEnabled(true);
	            play.setEnabled(true);
	            delete.setEnabled(true);
	            
	            Toast.makeText(getApplicationContext(), "Audio recorded successfully",Toast.LENGTH_LONG).show();
	         }
	      });
	      
	      play.setOnClickListener(new View.OnClickListener() {
	         @Override
	         public void onClick(View v) throws IllegalArgumentException,SecurityException,IllegalStateException {
	           corMediaPlayer = new MediaPlayer();
	            
	            try {
	            	corMediaPlayer.setDataSource(outputFile);
	            }
	            
	            catch (IOException e) {
	               e.printStackTrace();
	            }
	            
	            try {
	            	corMediaPlayer.prepare();
	            }
	            
	            catch (IOException e) {
	               e.printStackTrace();
	            }
	            
	            corMediaPlayer.start();
	            Toast.makeText(getApplicationContext(), "Playing audio", Toast.LENGTH_LONG).show();
	            
	         }
	      });
	      
	      delete.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				try {
					delete.setEnabled(false);
					play.setEnabled(false);
					
					if(corMediaPlayer.isPlaying()){
						corMediaPlayer.stop();
						}
					
					if(audioFile.exists()){
						audioFile.delete();
						Toast.makeText(getApplicationContext(), "File Deleted",Toast.LENGTH_LONG).show();
					}
					
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		});

		/*****************************************************************************************/


		ToggleSubMenu9=(Button)findViewById(R.id.toggleSubMenu9);
		ToggleSubMenu9.setOnClickListener(new OnClickListener() {
			

			@Override
			public void onClick(View v) {

				int currentRow=PublicData.secOneRowCount;
				currentRow++;
				PublicData.secOneRowCount=currentRow;
				int sectionNo=1;

				int nameTag=Integer.parseInt(""+sectionNo+""+currentRow+""+nameItem);
				int branchTag=Integer.parseInt(""+sectionNo+""+currentRow+""+branchItem);
				int accountTag=Integer.parseInt(""+sectionNo+""+currentRow+""+accountItem);
				int textViewTag=Integer.parseInt(""+sectionNo+"000"+currentRow);

				toggleSubmenuabletotoggle=true;

				if(toggleSubmenuabletotoggle)
				{
					ToggleSubMenu9CountClick++;
					LinearLayout parent = new LinearLayout(getApplicationContext());
					parent.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
					parent.setOrientation(LinearLayout.VERTICAL);
					parent.setTag(textViewTag);
					allView.add(parent);

					LinearLayout FirstBlockLayout =new LinearLayout(getApplicationContext());
					FirstBlockLayout.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT,10));
					FirstBlockLayout.setOrientation(LinearLayout.HORIZONTAL);
					FirstBlockLayout.setGravity(Gravity.RIGHT);
					FirstBlockLayout.setPadding(0, 0, 0, 25);
					
					String name=Ippa.getText().toString();
					String branch=Ipb.getText().toString();
					String account=Ips.getText().toString();
					String successor=UnUsedEditTextParent1.getText().toString();
					
					
					String text="פירוט"+" : "+name+"<br/>"+"סניף"+":"+branch+"<br/>"+"מספר חשבון"+" : "+account+"<br/>"+"שם היורש\\יורשים"+" : "+successor;

					TextView tv1 = new TextView(getApplicationContext());
					tv1.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,(float) 9.2));
					tv1.setText(Html.fromHtml(text));
					tv1.setGravity(Gravity.RIGHT);
					tv1.setTextColor(Color.parseColor("#000000"));
					tv1.setPadding(0, 0, 0, 40);
					tv1.setTypeface(null, Typeface.BOLD);
					
					Button deleteView=new Button(getApplicationContext());
					
					deleteView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,(float)0.8));
					deleteView.setText("מחק");
					deleteView.setTag(textViewTag);
					deleteView.setGravity(Gravity.CENTER);
					deleteView.setTextColor(Color.WHITE);
					deleteView.setBackground(delback);
					deleteView.setTypeface(null, Typeface.BOLD);
					deleteView.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Log.d("Button tag", v.getTag()+" is buttons");
							int buttonTag=(Integer) v.getTag();
							for(int i=0;i<allView.size();i++)
							{
								LinearLayout lay=allView.get(i);
								int tag=(Integer) lay.getTag();
								Log.d("delete text view", ""+tag);
								if(tag==buttonTag)
								{
									lay.setVisibility(LinearLayout.GONE);
									DataCat1.put(tag, "");
									
								}
							}
							
						}
					});


					section9++;
					FirstBlockLayout.addView(deleteView);
					FirstBlockLayout.addView(tv1);
					
					parent.addView(FirstBlockLayout);
					
					parent.setPadding(0, 25, 0, 25);

					NinthExpansionField.addView(parent);
					
					
					UnUsedEditText1id=1040;
					
					Ippa.setText("");
					Ipb.setText("");
					Ips.setText("");
					UnUsedEditTextParent1.setText("");
					DataCat1.put((Integer) Ippa.getTag(), Ippa.getText().toString());
					DataCat1.put((Integer) Ipb.getTag(), Ipb.getText().toString());
					DataCat1.put((Integer) Ips.getTag(), Ips.getText().toString());
					DataCat1.put(1040, "");
					DataCat1.put(textViewTag, text);
				}
			}
		});

		ToggleSubMenu10=(Button)findViewById(R.id.toggleSubMenu10);
		ToggleSubMenu10.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				int currentRow=PublicData.secTwoRowCount;
				currentRow++;
				PublicData.secTwoRowCount=currentRow;
				int sectionNo=2;

				int nameTag=Integer.parseInt(""+sectionNo+""+currentRow+""+nameItem);
				int branchTag=Integer.parseInt(""+sectionNo+""+currentRow+""+branchItem);
				int accountTag=Integer.parseInt(""+sectionNo+""+currentRow+""+accountItem);
				
				int textViewTag=Integer.parseInt(""+sectionNo+"000"+currentRow);

				toggleSubmenuabletotoggle=true;

				if(toggleSubmenuabletotoggle)
				{
					ToggleSubMenu10CountClick++;
					LinearLayout parent = new LinearLayout(getApplicationContext());
					parent.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
					parent.setOrientation(LinearLayout.VERTICAL);
					parent.setTag(textViewTag);
					allView.add(parent);

					LinearLayout FirstBlockLayout =new LinearLayout(getApplicationContext());
					FirstBlockLayout.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT,10));
					FirstBlockLayout.setOrientation(LinearLayout.HORIZONTAL);
					FirstBlockLayout.setGravity(Gravity.RIGHT);
					FirstBlockLayout.setPadding(0, 0, 0, 25);
					
					String name=Batbn.getText().toString();
					String branch=Babn.getText().toString();
					String account=Baan.getText().toString();
					String successor=UnUsedEditTextParent2.getText().toString();
					
					
					String text="פירוט"+" : "+name+"<br/>"+"סניף"+":"+branch+"<br/>"+"מספר חשבון"+" : "+account+"<br/>"+"שם היורש\\יורשים"+" : "+successor;

					Log.d("Section2", "" +text);

					TextView tv1 = new TextView(getApplicationContext());
					tv1.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,(float) 9.2));
					tv1.setText(Html.fromHtml(text));
					tv1.setGravity(Gravity.RIGHT);
					tv1.setTextColor(Color.parseColor("#000000"));
					tv1.setPadding(0, 0, 0, 40);
					tv1.setTypeface(null, Typeface.BOLD);
					
					Button deleteView=new Button(getApplicationContext());
					
					deleteView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,(float)0.8));
					deleteView.setText("מחק");
					deleteView.setTag(textViewTag);
					deleteView.setGravity(Gravity.CENTER);
					deleteView.setTextColor(Color.WHITE);
					deleteView.setBackground(delback);
					deleteView.setTypeface(null, Typeface.BOLD);
					deleteView.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Log.d("Button tag", v.getTag()+" is buttons");
							int buttonTag=(Integer) v.getTag();
							for(int i=0;i<allView.size();i++)
							{
								LinearLayout lay=allView.get(i);
								int tag=(Integer) lay.getTag();
								Log.d("delete text view", ""+tag);
								if(tag==buttonTag)
								{
									lay.setVisibility(LinearLayout.GONE);
									DataCat2.put(tag, "");
									
								}
							}
							
						}
					});


					section10++;
					//Now I am going to add dynamic layouts
					FirstBlockLayout.addView(deleteView);
					FirstBlockLayout.addView(tv1);
					parent.addView(FirstBlockLayout);
					parent.setPadding(0, 25, 0, 0);
					TenthExpansionField.addView(parent);
					
					UnUsedEditText2id=2040;
					Batbn.setText("");
					Babn.setText("");
					Baan.setText("");
					UnUsedEditTextParent2.setText("");
					DataCat2.put((Integer) Batbn.getTag(), Batbn.getText().toString());
					DataCat2.put((Integer) Babn.getTag(), Babn.getText().toString());
					DataCat2.put((Integer) Baan.getTag(), Baan.getText().toString());
					DataCat2.put(2040, "");
					DataCat2.put(textViewTag, text);
				}
			}
		});


		BackButton=(Button)findViewById(R.id.backButton);
		BackButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent=new Intent(CreateAWillActivity.this,WelcomeActivity.class);
				startActivity(intent);
			}
		});

		SubmitButton = (Button) findViewById(R.id.submitButton);
		SubmitButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				result="";
				resultp="";
				resultx="";
				//getting all the value to respective map
				getEditTextValue();

				Toast.makeText(getApplicationContext(), ""+numberofunusedbomboclick9, Toast.LENGTH_LONG).show();

				for(int i=1;i<=ToggleSubMenu9CountClick;i++)
				{
					for(int j=1;j<=numberofunusedbomboclick9;j++)
					{
						Toast.makeText(getApplicationContext(), ""+Section1Array[i][j], Toast.LENGTH_LONG).show();
					}
				}

				if("".equals(FullNameEditText.getText().toString()) ||
						"".equals(IdentityCardEditText.getText().toString()) ||
						"".equals(AddressEditText.getText().toString()) ||
						"".equals(PhoneEditText.getText().toString()))
				{

					if("".equals(FullNameEditText.getText().toString()))
					{
						FullNameEditText.setBackgroundResource(R.drawable.rectangle_edittext_redborder);
						Toast.makeText(getApplicationContext(), "הזן את שמך המלא", Toast.LENGTH_SHORT).show();
					}
					else {
						FullNameEditText.setBackgroundResource(R.drawable.rectangle_edittext);
					}
					if("".equals(IdentityCardEditText.getText().toString()))
					{
						IdentityCardEditText.setBackgroundResource(R.drawable.rectangle_edittext_redborder);
						Toast.makeText(getApplicationContext(), "אנא הזן תעודת זהות מספר", Toast.LENGTH_SHORT).show();
					}
					else {
						IdentityCardEditText.setBackgroundResource(R.drawable.rectangle_edittext);
					}
					if("".equals(AddressEditText.getText().toString()))
					{
						AddressEditText.setBackgroundResource(R.drawable.rectangle_edittext_redborder);
						Toast.makeText(getApplicationContext(), "אנא הכנס את כתובת", Toast.LENGTH_SHORT).show();
					}
					else {
						AddressEditText.setBackgroundResource(R.drawable.rectangle_edittext);
					}
					if("".equals(PhoneEditText.getText().toString()))
					{
						PhoneEditText.setBackgroundResource(R.drawable.rectangle_edittext_redborder);
						Toast.makeText(getApplicationContext(), "אנא הזן מספר טלפון", Toast.LENGTH_SHORT).show();
					}
					else {
						PhoneEditText.setBackgroundResource(R.drawable.rectangle_edittext);
					}
				}
				else{

					if(MultipleSuccessorCheckBorCheckedOrNot)
					{
						if("".equals(FullNameEditText.getText().toString()) ||
								"".equals(IdentityCardEditText.getText().toString()) ||
								"".equals(AddressEditText.getText().toString()) ||
								"".equals(PersonName.getText().toString())||
								"".equals(PhoneEditText.getText().toString()))
						{
							PersonName.setBackgroundResource(R.drawable.rectangle_edittext_redborder);
							Toast.makeText(getApplicationContext(), "נא למלא את כל השדות הדרושים", Toast.LENGTH_SHORT).show();
						}

						else{

							if(Agree){

								Intent intent = new Intent(CreateAWillActivity.this,SignatureAndVideoActivity.class);
								intent.putExtra("result", result);
								intent.putExtra("resultp", resultp);
								

								intent.putExtra("name", FullNameEditText.getText().toString());
								intent.putExtra("id", IdentityCardEditText.getText().toString());
								intent.putExtra("address", AddressEditText.getText().toString());
								intent.putExtra("phone", PhoneEditText.getText().toString());
								intent.putExtra("singleSuccessor", PersonName.getText().toString());
								intent.putExtra("multipleCheckString", multipleCheckString);
								
								resultx="<div dir=\"rtl\" style=\"font-family: Open Sans hebrew\" ><br/><br/>";
								resultx+="יורש יחיד"+" : "+PersonName.getText().toString();
								resultx+="</div>";
								
								intent.putExtra("resultx", resultx);

								PublicData.publicBundle=intent.getExtras();
								startActivity(intent);
							}
							else
							{
								Toast.makeText(getApplicationContext(), "Please Check Agree Terms and Conditions to move forward", Toast.LENGTH_LONG).show();
							}
						}
					}

					else
					{
						if(Agree)
						{
							resultx="<div dir=\"rtl\" style=\"font-family: Open Sans hebrew; text-align: right;\" >";
							if(getReatEstate().length()>10)
								resultx+=getReatEstate();
							if(getBank().length()>10)
								resultx+=getBank();
							
							if(getSavingsPlan().length()>10)
								resultx+=getSavingsPlan();
							if(getCash().length()>10)
								resultx+=getCash();
							
							if(getCompanyShare().length()>10)
								resultx+=getCompanyShare();
							if(getSecurityExchange().length()>10)
								resultx+=getSecurityExchange();
							
							if(getJewelry().length()>10)
								resultx+=getJewelry();
							if(getMovableProperty().length()>10)
								resultx+=getMovableProperty();
							
							if(getShareStock().length()>10)
								resultx+=getShareStock();
							if(getanotherProperty().length()>10)
								resultx+=getanotherProperty();
							
							

							Label objLabel2 =new Label(result,  0, 0, 504, 100, Font.getHelvetica(), 18, TextAlign.RIGHT);

							Toast.makeText(getApplicationContext(), multipleCheckString+"", Toast.LENGTH_SHORT).show();
							resultx+="</div>";
							Intent intent = new Intent(CreateAWillActivity.this,SignatureAndVideoActivity.class);
							intent.putExtra("result", result);
							intent.putExtra("resultp", resultp);
							intent.putExtra("resultx", resultx);

							intent.putExtra("name", FullNameEditText.getText().toString());
							intent.putExtra("id", IdentityCardEditText.getText().toString());
							intent.putExtra("address", AddressEditText.getText().toString());
							intent.putExtra("phone", PhoneEditText.getText().toString());

							intent.putExtra("multipleCheckString", multipleCheckString);
							PublicData.publicBundle=intent.getExtras();
							startActivity(intent);
						}
						else
						{
							Toast.makeText(getApplicationContext(), "Please Check Agree Terms and Conditions to move forward", Toast.LENGTH_LONG).show();
						}
					}
				}
			}
		});

		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
	}

	// end of oncreate 

	private void getEditTextValue() {
		for(int i=0; i < allEditText.size(); i++){
			int tag=(Integer) allEditText.get(i).getTag();
			String tagIndex [] =(""+tag).split("");
			String textTag=tagIndex[1];

			int swit=Integer.parseInt(textTag);

			if(tagIndex[2].equals("0")&& tagIndex[3].equals("0"))
				swit=10;

			Log.d("Adding text at DataDic"+swit,"tag :"+allEditText.get(i).getTag()+" text : "+allEditText.get(i).getText().toString() );
			switch(swit)
			{
			case 1 :
				DataCat1.put((Integer) allEditText.get(i).getTag(), allEditText.get(i).getText().toString());
				break;
			case 2 :
				DataCat2.put((Integer) allEditText.get(i).getTag(), allEditText.get(i).getText().toString());
				break;
			case 3 :
				DataCat3.put((Integer) allEditText.get(i).getTag(), allEditText.get(i).getText().toString());
				break;
			case 4 :
				DataCat4.put((Integer) allEditText.get(i).getTag(), allEditText.get(i).getText().toString());
				break;
			case 5 :
				DataCat5.put((Integer) allEditText.get(i).getTag(), allEditText.get(i).getText().toString());
				break;
			case 6 :
				DataCat6.put((Integer) allEditText.get(i).getTag(), allEditText.get(i).getText().toString());
				break;
			case 7 :
				DataCat7.put((Integer) allEditText.get(i).getTag(), allEditText.get(i).getText().toString());
				break;
			case 8 :
				DataCat8.put((Integer) allEditText.get(i).getTag(), allEditText.get(i).getText().toString());
				break;
			case 9 :
				DataCat9.put((Integer) allEditText.get(i).getTag(), allEditText.get(i).getText().toString());
				break;
			case 10 :
				DataCat10.put((Integer) allEditText.get(i).getTag(), allEditText.get(i).getText().toString());
				break;

			default :
				System.out.println("Invalid grade");
			}

		}	
	}

	private String tagtoName(int key)
	{
		String keyName="";
		String keyString []=(""+key).split("");
		int keyPosition=0;
		if(keyString.length>5)
		{
			keyPosition=4;
		}
		else {
			keyPosition=3;
		}
		String textTag=keyString[keyPosition];

		int swit=Integer.parseInt(textTag);

		Log.d("tag Name"," key "+key+ " string "+keyString+ "  length "+keyString.length+ "  key position "+ keyPosition+ " swit "+swit);

		switch(swit)
		{
		case 1 :
			keyName="פירוט";
			break;
		case 2 :
			keyName="סניף";
			break;
		case 3 :
			keyName="מספר חשבון";
			break;
		case 4 :
			keyName="שם היורש\\יורשים";
			break;
		default:
			break;
		}
		Log.d("tag Name"," keyName "+keyName);
		return keyName;
	}
	
	TreeMap< Integer, String> getSortedMap(Map<Integer, String>dataCat )
	{
		Log.d("map size at sorted", dataCat.isEmpty()+" "+dataCat.size());
		
		for (Entry<Integer, String> entry : dataCat.entrySet()) {
		    Integer key = entry.getKey();
		    String value = entry.getValue();
		    Log.d(" content at amp ","key : " + key + "; value :" + value);
		}
		
		Map<Integer, String> treeMap = new TreeMap<Integer, String>(
				new Comparator<Integer>() {

				@Override
				public int compare(Integer o1, Integer o2) {
					return o2.compareTo(o1);
				}
			});
		try {
			treeMap.putAll(dataCat);
		} catch (Exception e) {
			Log.d("map Exception", e.getMessage());
		}
		
		return (TreeMap<Integer, String>) treeMap;
	}
	
	private String getReatEstate()
	{
		//DataCat1
		String realEstate="";
		String data="";
		String preData="";
		
		Map<Integer, String> treeMap = getSortedMap(DataCat1);
		
		for (Map.Entry<Integer, String> entry : treeMap.entrySet())
		{
			if(entry.getKey().toString().length()<5)
			{
				String tagName="";
				if(entry.getValue().length()>2){
					tagName=tagtoName(entry.getKey());
					data+="<b>"+tagName+"</b> : "+entry.getValue()+"<br/>";
				}
				Log.d("All value of Bank"," tag "+entry.getKey()+" tagName : "+tagName+"  value "+entry.getValue());
			}
			else 
			{
				if(entry.getValue().length()>5)
					preData+=entry.getValue()+"<br/>";
			}
		}
		if(data.length()>10 || preData.length()>10)
		{
			realEstate+="<b><u>נדל\"ן</u></b><br/>";
			realEstate+=data+" "+preData;
			if(noteMap.get(9) != null)
			{
				realEstate+="הערות"+" : "+noteMap.get(9);
			}
				
			realEstate+="<br/><br/>";
		}
		
		return realEstate;
	}
	private String getBank()
	{
		//DataCat2
		String realEstate="";
		String data="";
		String preData="";
		Map<Integer, String> treeMap = getSortedMap(DataCat2);
		for (Map.Entry<Integer, String> entry : treeMap.entrySet())
		{
			if(entry.getKey().toString().length()<5)
			{
				String tagName="";
				if(entry.getValue().length()>2){
					tagName=tagtoName(entry.getKey());
					data+="<b>"+tagName+"</b> : "+entry.getValue()+"<br/>";
				}
				Log.d("All value of Bank"," tag "+entry.getKey()+" tagName : "+tagName+"  value "+entry.getValue());
			}
			else 
			{
				if(entry.getValue().length()>5)
					preData+=entry.getValue()+"<br/>";
			}
		}
		if(data.length()>10 || preData.length()>10)
		{
			realEstate+="<b><u>בַּנק</u></b><br/>";
			realEstate+=data+" "+preData;
			if(noteMap.get(10) != null)
			{
				realEstate+="הערות"+" : "+noteMap.get(10);
			}
				
			realEstate+="<br/><br/>";
		}
		
		return realEstate;
	}
	private String getSavingsPlan()
	{
		//DataCat3
		String realEstate="";
		String data="";
		String preData="";
		Map<Integer, String> treeMap = getSortedMap(DataCat3);
		for (Map.Entry<Integer, String> entry : treeMap.entrySet())
		{
			if(entry.getKey().toString().length()<5)
			{
				String tagName="";
				if(entry.getValue().length()>2){
					tagName=tagtoName(entry.getKey());
					data+="<b>"+tagName+"</b> : "+entry.getValue()+"<br/>";
				}
				Log.d("All value of Bank"," tag "+entry.getKey()+" tagName : "+tagName+"  value "+entry.getValue());
			}
			else 
			{
				if(entry.getValue().length()>5)
					preData+=entry.getValue()+"<br/>";
			}
		}

		if(data.length()>10 || preData.length()>10)
		{
			realEstate+="<b><u>תכנית חסכון</u></b><br/>";
			realEstate+=data+" "+preData;
			if(noteMap.get(1) != null)
			{
				realEstate+="הערות"+" : "+noteMap.get(1);
			}
				
			realEstate+="<br/><br/>";
		}

		return realEstate;
	}
	private String getCash()
	{
		//DataCat4
		String realEstate="";
		String data="";
		String preData="";
		Map<Integer, String> treeMap = getSortedMap(DataCat4);
		for (Map.Entry<Integer, String> entry : treeMap.entrySet())
		{if(entry.getKey().toString().length()<5)
		{
			String tagName="";
			if(entry.getValue().length()>2){
				tagName=tagtoName(entry.getKey());
				data+="<b>"+tagName+"</b> : "+entry.getValue()+"<br/>";
			}
			Log.d("All value of Bank"," tag "+entry.getKey()+" tagName : "+tagName+"  value "+entry.getValue());
		}
		else 
		{
			if(entry.getValue().length()>5)
				preData+=entry.getValue()+"<br/>";
		}
		}
		if(data.length()>10 || preData.length()>10)
		{
			realEstate+="<b><u>מזומנים</u></b><br/>";
			realEstate+=data+" "+preData;
			if(noteMap.get(2) != null)
			{
				realEstate+="הערות"+" : "+noteMap.get(2);
			}
				
			realEstate+="<br/><br/>";
		}
		
		return realEstate;
	}

	private String getCompanyShare()
	{
		//DataCat5
		String realEstate="";
		String data="";
		String preData="";
		Map<Integer, String> treeMap = getSortedMap(DataCat5);
		for (Map.Entry<Integer, String> entry : treeMap.entrySet())
		{
			if(entry.getKey().toString().length()<5)
			{
				String tagName="";
				if(entry.getValue().length()>2){
					tagName=tagtoName(entry.getKey());
					data+="<b>"+tagName+"</b> : "+entry.getValue()+"<br/>";
				}
				Log.d("All value of Bank"," tag "+entry.getKey()+" tagName : "+tagName+"  value "+entry.getValue());
			}
			else 
			{
				if(entry.getValue().length()>5)
					preData+=entry.getValue()+"<br/>";
			}
		}
		if(data.length()>10 || preData.length()>10)
		{
			realEstate+="<b><u>מניות בחברה</u></b><br/>";
			realEstate+=data+" "+preData;
			
			if(noteMap.get(3) != null)
			{
				realEstate+="הערות"+" : "+noteMap.get(3);
			}
				
			realEstate+="<br/><br/>";
		}
		
		return realEstate;
	}
	private String getSecurityExchange()
	{
		//DataCat6
		String realEstate="";
		String data="";	
		String preData="";
		Log.d("map size", DataCat6.isEmpty()+" "+DataCat6.size());
		Map<Integer, String> treeMap = getSortedMap(DataCat6);
		for (Map.Entry<Integer, String> entry : treeMap.entrySet())
		{
			if(entry.getKey().toString().length()<5)
			{
				String tagName="";
				if(entry.getValue().length()>2){
					tagName=tagtoName(entry.getKey());
					data+="<b>"+tagName+"</b> : "+entry.getValue()+"<br/>";
				}
				Log.d("All value of Bank"," tag "+entry.getKey()+" tagName : "+tagName+"  value "+entry.getValue());
			}
			else 
			{
				if(entry.getValue().length()>5)
					preData+=entry.getValue()+"<br/>";
			}
		}
		if(data.length()>10 || preData.length()>10)
		{
			realEstate+="<b><u>ניירות ערך ומט\"ח</u></b><br/>";
			realEstate+=data+" "+preData;
			
			if(noteMap.get(4) != null)
			{
				realEstate+="הערות"+" : "+noteMap.get(4);
			}
				
			realEstate+="<br/><br/>";
		}
		
		return realEstate;
	}
	private String getJewelry()
	{
		//DataCat7
		String realEstate="";
		String data="";
		String preData="";
		Map<Integer, String> treeMap = getSortedMap(DataCat7);
		for (Map.Entry<Integer, String> entry : treeMap.entrySet())
		{
			if(entry.getKey().toString().length()<5)
			{
				String tagName="";
				if(entry.getValue().length()>2){
					tagName=tagtoName(entry.getKey());
					data+="<b>"+tagName+"</b> : "+entry.getValue()+"<br/>";
				}
				Log.d("All value of Bank"," tag "+entry.getKey()+" tagName : "+tagName+"  value "+entry.getValue());
			}
			else 
			{
				if(entry.getValue().length()>5)
					preData+=entry.getValue()+"<br/>";
			}
		}
		if(data.length()>10 || preData.length()>10)
		{
			realEstate+="<b><u>תכשיטים</u></b><br/>";
			realEstate+=data+" "+preData;
			realEstate+=" <br/><br/>";
		}

		return realEstate;
	}
	private String getMovableProperty()
	{
		//DataCat8
		String realEstate="";
		String data="";	
		String preData="";
		Map<Integer, String> treeMap = getSortedMap(DataCat8);
		for (Map.Entry<Integer, String> entry : treeMap.entrySet())
		{
			if(entry.getKey().toString().length()<5)
			{
				String tagName="";
				if(entry.getValue().length()>2){
					tagName=tagtoName(entry.getKey());
					data+="<b>"+tagName+"</b> : "+entry.getValue()+"<br/>";
				}
				Log.d("All value of Bank"," tag "+entry.getKey()+" tagName : "+tagName+"  value "+entry.getValue());
			}
			else 
			{
				if(entry.getValue().length()>5)
					preData+=entry.getValue()+"<br/>";
			}
		}
		if(data.length()>10 || preData.length()>10)
		{
			realEstate+="<b><u>מטלטלין</u></b><br/>";
			realEstate+=data+" "+preData;
			realEstate+=" <br/><br/>";
		}
		return realEstate;
	}
	private String getShareStock()
	{
		//DataCat9
		String realEstate="";
		String data="";	
		String preData="";
		Map<Integer, String> treeMap = getSortedMap(DataCat9);
		for (Map.Entry<Integer, String> entry : treeMap.entrySet())
		{
			if(entry.getKey().toString().length()<5)
			{
				String tagName="";
				if(entry.getValue().length()>2){
					tagName=tagtoName(entry.getKey());
					data+="<b>"+tagName+"</b> : "+entry.getValue()+"<br/>";
				}
				Log.d("All value of Bank"," tag "+entry.getKey()+" tagName : "+tagName+"  value "+entry.getValue());
			}
			else 
			{
				if(entry.getValue().length()>5)
					preData+=entry.getValue()+"<br/>";
			}
		}
		if(data.length()>10 || preData.length()>10)
		{
			realEstate+="<b><u>מניות בבורסה</u></b><br/>";
			realEstate+=data+" "+preData;
			realEstate+=" <br/><br/>";
		}
		return realEstate;
	}
	private String getanotherProperty()
	{
		//DataCat10
		String realEstate="";
		String data="";	
		String preData="";
		Map<Integer, String> treeMap = getSortedMap(DataCat10);
		for (Map.Entry<Integer, String> entry : treeMap.entrySet())
		{
			if(entry.getKey().toString().length()<5)
			{
				String tagName="";
				if(entry.getValue().length()>2){
					tagName=tagtoName(entry.getKey());
					data+="<b>"+tagName+"</b> : "+entry.getValue()+"<br/>";
				}
				Log.d("All value of Bank"," tag "+entry.getKey()+" tagName : "+tagName+"  value "+entry.getValue());
			}
			else 
			{
				if(entry.getValue().length()>5)
					preData+=entry.getValue()+"<br/>";
			}
		}
		if(data.length()>10 || preData.length()>10)
		{
			realEstate+="<b><u>רכוש נוסף</u></b><br/>";
			realEstate+=data+" "+preData;
			realEstate+=" <br/><br/>";
		}
		return realEstate;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.create_awill, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
				&& keyCode == KeyEvent.KEYCODE_BACK
				&& event.getRepeatCount() == 0) {
			Log.d("CDA", "onKeyDown Called");
			onBackPressed();
			return true; 
		}
		return super.onKeyDown(keyCode, event);
	}

	// hide key board on 
	public static void hideSoftKeyboard(Activity activity) {
	    InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
	    inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
	}
	public void setupUI(View view) {

	    //Set up touch listener for non-text box views to hide keyboard.
	    if(!(view instanceof EditText)) {

	        view.setOnTouchListener(new OnTouchListener() {

	            public boolean onTouch(View v, MotionEvent event) {
	                hideSoftKeyboard(CreateAWillActivity.this);
	                return false;
	            }

	        });
	    }

	    //If a layout container, iterate over children and seed recursion.
	    if (view instanceof ViewGroup) {

	        for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

	            View innerView = ((ViewGroup) view).getChildAt(i);

	            setupUI(innerView);
	        }
	    }
	}
// hide key board end
	public static int convertPixelsToDp(float px, Context context)
	{
		Resources resources = context.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		int dp = (int) (px / (metrics.densityDpi / 160f));
		return dp;
	}
}