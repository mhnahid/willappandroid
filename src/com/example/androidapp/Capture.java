package com.example.androidapp;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.util.AttributeSet;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class Capture extends Activity { 

    LinearLayout mContent;
    signature mSignature;
    Button mClear, mGetSign, mCancel;
    public static String tempDir;
    public int count = 1;
    public String current = null;
    private Bitmap mBitmap;
    View mView;
    File mypath;
    private String uniqueId;
    Button BackButotn;
   String Checkplayable="";
   
   String result="";
   String resultp="";
   
   @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_capture);
        
    	
		Bundle bundle = getIntent().getExtras();
	    result = bundle.getString("result");
	    resultp = bundle.getString("resultp");
	    PublicData.preViewSent=true;
        
        String checkplayable=getIntent().getStringExtra("checkvideo");
        Checkplayable=checkplayable;
        BackButotn=(Button)findViewById(R.id.backButton);
        BackButotn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				 Intent myintent=new Intent(Capture.this, SignatureAndVideoActivity.class);
		            startActivity(myintent);
			}
		});
        
        tempDir="/sdcard/";
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        File directory = cw.getDir(getResources().getString(R.string.external_dir), Context.MODE_PRIVATE);
        uniqueId="signature";
        current = uniqueId + ".png";
        mypath= new File(directory,current);
        mContent = (LinearLayout) findViewById(R.id.linearLayout);
        mSignature = new signature(this, null);
        mSignature.setBackgroundColor(Color.WHITE);
        mContent.addView(mSignature, LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
        mClear = (Button)findViewById(R.id.clear);
        mGetSign = (Button)findViewById(R.id.getsign);
        mGetSign.setEnabled(false);
        mCancel = (Button)findViewById(R.id.cancel);
        mView = mContent;

        mClear.setOnClickListener(new OnClickListener() 
        {        
            public void onClick(View v) 
            {
                Log.v("log_tag", "Panel Cleared");
                mSignature.clear();
                mGetSign.setEnabled(false);
            }
        });

        mGetSign.setOnClickListener(new OnClickListener() 
        {        
            public void onClick(View v) 
            {
                Log.v("log_tag", "Panel Saved");
                boolean error = captureSignature();
                if(!error){
                	
                    mView.setDrawingCacheEnabled(true);
                    mSignature.save(mView);
                    Intent myintent=new Intent(Capture.this, SignatureAndVideoActivity.class).putExtra("checksignature", "done");
		            myintent.putExtra("checkvideo", Checkplayable);
		            myintent.putExtra("result", result);
			        myintent.putExtra("resultp", resultp);
		            
		            PublicData.publicBundle.putString("checksignature", "done");
		            PublicData.publicBundle.putString("checkvideo", Checkplayable);

                    startActivity(myintent);
                    finish();
                }
            }
        });

        mCancel.setOnClickListener(new OnClickListener() 
        {        
            public void onClick(View v) 
            {
                Log.v("log_tag", "Panel Canceled");
                Bundle b = new Bundle();
                b.putString("status", "cancel");
                Intent intent = new Intent();
                intent.putExtras(b);
                setResult(RESULT_OK,intent);  
                finish();
            }
        });
    }

    @Override
    protected void onDestroy() {
        Log.w("GetSignature", "onDestory");
        super.onDestroy();
    }

    private boolean captureSignature() {

        boolean error = false;
        String errorMessage = "";
        return error;
    }


    public class signature extends View 
    {
        private static final float STROKE_WIDTH = 5f;
        private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
        private Paint paint = new Paint();
        private Path path = new Path();

        private float lastTouchX;
        private float lastTouchY;
        private final RectF dirtyRect = new RectF();

        public signature(Context context, AttributeSet attrs) 
        {
            super(context, attrs);
            paint.setAntiAlias(true);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeWidth(STROKE_WIDTH);
        }

        public void save(View v) 
        {
            Log.v("log_tag", "Width: " + v.getWidth());
            Log.v("log_tag", "Height: " + v.getHeight());
            String filePath = PublicData.signPath;
            if(mBitmap == null)
            {
                mBitmap =  Bitmap.createBitmap (mContent.getWidth(), mContent.getHeight(), Bitmap.Config.RGB_565);;
            }
            Canvas canvas = new Canvas(mBitmap);
            try 
            {
                FileOutputStream mFileOutStream = new FileOutputStream(new File(filePath));

                v.draw(canvas); 
                mBitmap.compress(Bitmap.CompressFormat.JPEG, 90, mFileOutStream); 
                mFileOutStream.flush();
                mFileOutStream.close();
                String url = Images.Media.insertImage(getContentResolver(), mBitmap, "signature", null);
                Log.v("log_tag","url: " + url);

                
                
                
                /*
                String[] projection = new String[]{
                	    MediaStore.Images.ImageColumns._ID,
                	    MediaStore.Images.ImageColumns.DATA,
                	    MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME,
                	    MediaStore.Images.ImageColumns.DATE_TAKEN,
                	    MediaStore.Images.ImageColumns.MIME_TYPE
                	    };
                	final Cursor cursor = getContentResolver()
                	        .query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null, 
                	               null, MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC");

                	// Put it in the image view
                	if (cursor.moveToFirst()) {
                	    String imageLocation = cursor.getString(1);
                	    File imageFile = new File(imageLocation);
                	    if (imageFile.exists()) {   // TODO: is there a better way to do this?

                            Toast.makeText(getApplicationContext(), imageLocation +"", Toast.LENGTH_LONG).show();
                	    }
                	}
                */
                
               
            }
            catch(Exception e) 
            { 
                Log.v("log_tag", e.toString()); 
            } 
        }

        public void clear() 
        {
            path.reset();
            invalidate();
        }

        @Override
        protected void onDraw(Canvas canvas) 
        {
            canvas.drawPath(path, paint);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) 
        {
            float eventX = event.getX();
            float eventY = event.getY();
            mGetSign.setEnabled(true);

            switch (event.getAction()) 
            {
            case MotionEvent.ACTION_DOWN:
                path.moveTo(eventX, eventY);
                lastTouchX = eventX;
                lastTouchY = eventY;
                return true;

            case MotionEvent.ACTION_MOVE:

            case MotionEvent.ACTION_UP:

                resetDirtyRect(eventX, eventY);
                int historySize = event.getHistorySize();
                for (int i = 0; i < historySize; i++) 
                {
                    float historicalX = event.getHistoricalX(i);
                    float historicalY = event.getHistoricalY(i);
                    expandDirtyRect(historicalX, historicalY);
                    path.lineTo(historicalX, historicalY);
                }
                path.lineTo(eventX, eventY);
                break;

            default:
                debug("Ignored touch event: " + event.toString());
                return false;
            }

            invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                    (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

            lastTouchX = eventX;
            lastTouchY = eventY;

            return true;
        }

        private void debug(String string){
        }

        private void expandDirtyRect(float historicalX, float historicalY) 
        {
            if (historicalX < dirtyRect.left) 
            {
                dirtyRect.left = historicalX;
            } 
            else if (historicalX > dirtyRect.right) 
            {
                dirtyRect.right = historicalX;
            }

            if (historicalY < dirtyRect.top) 
            {
                dirtyRect.top = historicalY;
            } 
            else if (historicalY > dirtyRect.bottom) 
            {
                dirtyRect.bottom = historicalY;
            }
        }

        private void resetDirtyRect(float eventX, float eventY) 
        {
            dirtyRect.left = Math.min(lastTouchX, eventX);
            dirtyRect.right = Math.max(lastTouchX, eventX);
            dirtyRect.top = Math.min(lastTouchY, eventY);
            dirtyRect.bottom = Math.max(lastTouchY, eventY);
        }
    }
  //if back button is pressed
  	@Override
  	public boolean onKeyDown(int keyCode, KeyEvent event)  {
  	    if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
  	            && keyCode == KeyEvent.KEYCODE_BACK
  	            && event.getRepeatCount() == 0) {
  	        Log.d("CDA", "onKeyDown Called");
  	        onBackPressed();
  	        return true; 
  	    }
  	    return super.onKeyDown(keyCode, event);
  	}
  	@Override
  	public void onBackPressed() {
  	}
  	
}