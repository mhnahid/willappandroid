package com.example.androidapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ScrollingView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

public class TermsofServicesAcivity extends Activity {

	Button BackButton;
	ScrollView sView;
	TextView tv1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_termsof_services_acivity);
		
		sView = (ScrollView)findViewById(R.id.scrollView);
		sView.setVerticalScrollBarEnabled(false);
		sView.setHorizontalScrollBarEnabled(false);
		
		
		BackButton=(Button) findViewById(R.id.backButton);
		BackButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(TermsofServicesAcivity.this,WelcomeActivity.class);
				startActivity(intent);
				
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.termsof_services_acivity, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	//if back button is pressed
		@Override
		public boolean onKeyDown(int keyCode, KeyEvent event)  {
		    if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
		            && keyCode == KeyEvent.KEYCODE_BACK
		            && event.getRepeatCount() == 0) {
		        Log.d("CDA", "onKeyDown Called");
		        onBackPressed();
		        return true; 
		    }
		    return super.onKeyDown(keyCode, event);
		}

}
