package com.example.androidapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class WelcomeActivity extends Activity {

	Button MakeAWill;
	Button AboutTheFirmButton;
	Button TermsAndConditionsButton;
	Button MakeAWillTag;
	TextView AboutTheFirmTagTextView;
	TextView TermsAndConditionsTagTextView;
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_welcome);
		
		MakeAWillTag=(Button)findViewById(R.id.makeAWillButtonTag);
		MakeAWillTag.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent =new Intent(WelcomeActivity.this,CreateAWillActivity.class);
				startActivity(intent);
				
			}
		});
		AboutTheFirmTagTextView=(TextView)findViewById(R.id.aboutTheFirmTagTextView);
		AboutTheFirmTagTextView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent=new Intent(WelcomeActivity.this,AboutFirmActivity.class);
				startActivity(intent);
				
			}
		});
		TermsAndConditionsTagTextView=(TextView)findViewById(R.id.termsAndConditionsTagTextView);
		TermsAndConditionsTagTextView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent =new Intent(WelcomeActivity.this,TermsofServicesAcivity.class);
				startActivity(intent);
				
			}
		});
		
		
		
		TermsAndConditionsButton=(Button)findViewById(R.id.termsAndConditionsButton);
		TermsAndConditionsButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent =new Intent(WelcomeActivity.this,TermsofServicesAcivity.class);
				startActivity(intent);
				
			}
		});
		
		
		
		AboutTheFirmButton=(Button)findViewById(R.id.aboutTheFirmButton);
		AboutTheFirmButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent=new Intent(WelcomeActivity.this,AboutFirmActivity.class);
				startActivity(intent);
				
			}
		});
		MakeAWill=(Button)findViewById(R.id.makeAWillButton);
		MakeAWill.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent =new Intent(WelcomeActivity.this,CreateAWillActivity.class);
				startActivity(intent);
				
			}
		});
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.welcome, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	//if back button is pressed
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)  {
	    if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
	            && keyCode == KeyEvent.KEYCODE_BACK
	            && event.getRepeatCount() == 0) {
	        Log.d("CDA", "onKeyDown Called");
	        onBackPressed();
	        return true; 
	    }
	    return super.onKeyDown(keyCode, event);
	}


	@Override
	public void onBackPressed() {
	   //Log.d("CDA", "onBackPressed Called");
	   //Intent setIntent = new Intent(Intent.ACTION_MAIN);
	   //setIntent.addCategory(Intent.CATEGORY_HOME);
	   //setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	   //startActivity(setIntent);
	}
	
	
}
