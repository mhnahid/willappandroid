package com.example.androidapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class PrepareAWillActivity extends Activity {
	Button BackButton;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_prepare_awill);
		BackButton = (Button)findViewById(R.id.backButton);
		BackButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
//				this.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK)); 
//				Intent intent=new Intent(PrepareAWillActivity.this,CreateAWillActivity.class);
//				startActivity(intent);
			}
		});
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.prepare_awill, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)  {
	    if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
	            && keyCode == KeyEvent.KEYCODE_BACK
	            && event.getRepeatCount() == 0) {
	        Log.d("CDA", "onKeyDown Called");
	        onBackPressed();
	        return true; 
	    }
	    return super.onKeyDown(keyCode, event);
	}


	@Override
	public void onBackPressed() {
	   //Log.d("CDA", "onBackPressed Called");
	   //Intent setIntent = new Intent(Intent.ACTION_MAIN);
	   //setIntent.addCategory(Intent.CATEGORY_HOME);
	   //setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	   //startActivity(setIntent);
	}
	
}
