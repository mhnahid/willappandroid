package com.example.androidapp;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Toast;



import java.nio.charset.Charset;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerFontProvider;
import com.itextpdf.tool.xml.css.StyleAttrCSSResolver;
import com.itextpdf.tool.xml.html.CssAppliers;
import com.itextpdf.tool.xml.html.CssAppliersImpl;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.PdfWriterPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;

public class PaymentGatewayActivity extends Activity {

	Button BackButton;
	Button SubmitButton;
	ScrollView sView;
	String result="";
	String resultp="";

	String resultx="";
	String htmlText="";
	String pdfHtml="";

	String MailBody="";
	String userMailAddress="";

	WebView paymemtVew;

	private Mail m;
	List<String> filePaths = new ArrayList<String>();
	ArrayList<Uri> uris = new ArrayList<Uri>();

	final ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

	@SuppressLint("SimpleDateFormat")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_payment_gateway);

		paymemtVew=(WebView) findViewById(R.id.webView1);
		paymemtVew.setWebViewClient(new MyWebViewClient());
		paymemtVew.getSettings().setJavaScriptEnabled(true);
//		paymemtVew.loadUrl("https://direct.tranzila.com/ttxhedvaweise");
		
		paymemtVew.loadUrl("http://192.168.0.103:88/myweb/index1.php");

		//		
		m = new Mail("cortwotect@gmail.com", "Strong1!");
		Bundle bundle =PublicData.publicBundle;// getIntent().getExtras();
		result = bundle.getString("result");
		resultp = bundle.getString("resultp");
		resultx = bundle.getString("resultx");
		
		Bundle htmlBundel=getIntent().getExtras();
		htmlText=htmlBundel.getString("htmlText");
		pdfHtml=htmlBundel.getString("pdfHtml");


		BackButton=(Button)findViewById(R.id.backButton);
		BackButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

		Toast.makeText(getApplicationContext(), "Connecting...", Toast.LENGTH_SHORT).show();
	}


	private String getEmail(){
		//Log.d(TAG, "Start - getEmail()");
		String email= "";

		try 
		{
			//Log.d(TAG, "Start - getEmail()");
			Account[] accounts=AccountManager.get(this).getAccountsByType("com.google");
			//Log.d(TAG, "Email Size : " + accounts.length);

			for (Account account : accounts) {

				String possibleEmail = account.name;
				String type = account.type;

				if (type.equals("com.google")) {
					email = possibleEmail;
					break;
				}
			}
		} catch (Exception e) {
			//Log.d(TAG, "getEmail() : "+e.getMessage());		
		}

		return email;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.payment_gateway, menu);
		return true;
	}


	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}


	// web view checking and class
	private class MyWebViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			Log.d("********** web shouldOverrideUrlLoading url", "current "+url);

//			if(url.equalsIgnoreCase("http://192.168.0.100:88/myweb/test.php/"))
//			{
//				Log.d("mail", "sending mail");
//				userMailAddress=PublicData.toMail;
//				setMail();
//				//sendmail();
//			}
			return true;
		}
		
		@Override
		public void onPageFinished (WebView view, String url)
		{
			Log.d("********** web url onPageFinished", "current "+url);
			
			if(url.equalsIgnoreCase("http://192.168.0.103:88/myweb/test.php"))
			{
				Log.d("mail", "sending mail");
				userMailAddress=PublicData.toMail;
				setMail();
				//sendmail();
			}
		}
		
		@Override
		public void onPageStarted (WebView view, 
                String url, 
                Bitmap favicon)
                
                {
			Log.d("********** web url onPageStarted", "current "+url);
		}

		// running thread to send mail
		void mailRun(){
			Thread thread = new Thread(new Runnable()
			{
				@Override
				public void run() 
				{
					try 
					{
						File signFile= new File(PublicData.signPath);
						File videoFile= new File(PublicData.videoPath);
						File pdfFile= new File(PublicData.pdfPath);

						if(signFile.exists())
						{
							m.addAttachment(PublicData.signPath);
							Log.d("mail Attachment", "sign attached");
						}

						if(videoFile.exists())
						{
							m.addAttachment(PublicData.videoPath);
							Log.d("mail Attachment", "video attached");
						}

						if(pdfFile.exists())
						{
							m.addAttachment(PublicData.pdfPath);
							Log.d("mail Attachment", "pdf attached");
						}

						if(m.send()) {
							// success
							Log.d("mail", "success");

							if(signFile.exists())
								signFile.delete();
							if(videoFile.exists())
								videoFile.delete();
							if(pdfFile.exists())
								pdfFile.delete();
							//Toast.makeText(PaymentGatewayActivity.this, "Email was sent successfully.", Toast.LENGTH_LONG).show();
						} else
						{
							// failure
							Log.d("mail", "failure");
							// Toast.makeText(getApplicationContext(), "Email was not sent.", Toast.LENGTH_LONG).show();
						}

					} 
					catch (Exception e)
					{
						Log.d("mail", "ex"+e.getMessage());
						//Toast.makeText(MainActivity.this, "There was a problem sending the email.", Toast.LENGTH_LONG).show();
						// Toast.makeText(getApplicationContext(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
					}
				}
			});
			thread.start(); 

		}

		//  adding all mail info
		void sendmail()
		{
			Log.d("mail", "At  sending mail");
			userMailAddress=getEmail();
			if(userMailAddress.length()<2)
			{
				// get prompts.xml view
				LayoutInflater li = LayoutInflater.from(PaymentGatewayActivity.this);
				View promptsView = li.inflate(R.layout.mailalert, null);

				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(PaymentGatewayActivity.this);

				// set prompts.xml to alertdialog builder
				alertDialogBuilder.setView(promptsView);

				final EditText userInput = (EditText) promptsView
						.findViewById(R.id.editTextDialogUserInput);

				// set dialog message
				alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton("OK",
						new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,
							int id) {
						// get user input and set it to result
						// edit text
						userMailAddress=userInput.getText().toString();
						setMail();
						Log.d("ok mail ", ""+userMailAddress);
					}
				})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,
							int id) {
						dialog.cancel();
					}
				});

				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();
			}
			else
			{
				setMail();
			}
			Log.d("User mail", userMailAddress);
		}

		void setMail()
		{
			Log.d("mail", "At  setting mail");
			m = new Mail(PublicData.FromMail, PublicData.FromMailPass);
			MailBody=htmlText;

			m.setFrom(PublicData.FromMail); // who is sending the email
			m.setSubject(PublicData.paidSub);
			m.setBody(""+MailBody);

			Log.d("User mail to ", userMailAddress);
			String[] toArr = {userMailAddress};
			// This is an array, you can add more emails, just separate them with a coma
			m.setTo(toArr); // load array to setT

			try {
				newPdf();
				//createPdf();
				mailRun();
			} catch (Exception e) {
				Log.d(" Exception mail", e.getMessage());
			}
		}

		void newPdf()
		{
			Log.d("PDf creation", " at itextpdf pdf createPdf");
			try {
				
				Document document = new  Document();
				PdfWriter pdfWriter=PdfWriter.getInstance(document, new FileOutputStream(PublicData.pdfPath));
				
				document.open();
				document.addAuthor("Cor2tect");
				document.addCreator("Cor2tect");
				document.addSubject("Thanks for your support");
				document.addCreationDate();
				document.addTitle("Please read this");


				String str =pdfHtml;
				str=str.replace("</br>", "br /");

				InputStream in = new ByteArrayInputStream(str.getBytes("UTF-8"));

				CSSResolver cssResolver = new StyleAttrCSSResolver();

				XMLWorkerFontProvider fontProvider = new XMLWorkerFontProvider(XMLWorkerFontProvider.DONTLOOKFORFONTS);
				
				fontProvider.register("fonts/OpenSansHebrew-Regular.ttf");
				
				CssAppliers cssAppliers = new CssAppliersImpl(fontProvider);
				HtmlPipelineContext htmlContext = new HtmlPipelineContext(cssAppliers);
				htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());

				// Pipelines
				PdfWriterPipeline pdf = new PdfWriterPipeline(document, pdfWriter);
				HtmlPipeline html = new HtmlPipeline(htmlContext, pdf);
				CssResolverPipeline css = new CssResolverPipeline(cssResolver, html);

				// XML Worker
				XMLWorker worker = new XMLWorker(css, true);
				XMLParser p = new XMLParser(worker);
				p.parse(in, Charset.forName("UTF-8"));;
				

				//XMLWorkerHelper.getInstance().parseXHtml(pdfWriter, document, in,Charset.forName("UTF-8"));
				document.close();
				in.close();
				Log.d("PDf creation", "pdf creation successfull");
			}
			catch (Exception e) {
				e.printStackTrace();
				Log.d("PDf creation", "pdf creation failed");
			}

		}

	}	
}
