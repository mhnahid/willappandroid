package com.example.androidapp;

import java.io.IOException;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;


public class VideoCapture extends Activity implements OnClickListener, SurfaceHolder.Callback {
   
	
	MediaRecorder recorder;
    SurfaceHolder holder;
    boolean recording = false;
    
    String result="";
    String resultp="";
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        PublicData.preViewSent=true;
        recorder = new MediaRecorder();
        initRecorder();
        setContentView(R.layout.activity_video_capture);

        Bundle bundle = getIntent().getExtras();
	    result = bundle.getString("result");
	    resultp=bundle.getString("resultp");
	    
		String multipleCheckString=PublicData.publicBundle.getString("multipleCheckString");
	   Log.d("name ", ""+multipleCheckString);
        
        
        SurfaceView cameraView = (SurfaceView) findViewById(R.id.CameraView);
        holder = cameraView.getHolder();
        holder.addCallback(this);
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        cameraView.setClickable(true);
        cameraView.setOnClickListener(this);
        Toast.makeText(getApplicationContext(), "tap to start & stop", Toast.LENGTH_LONG).show();

        //cameraView.performClick();
        
        
    }
    
    private void initRecorder() {
        recorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
        recorder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);
        CamcorderProfile cpHigh = CamcorderProfile
                .get(CamcorderProfile.QUALITY_HIGH);
        recorder.setVideoEncodingBitRate(690000 );
        recorder.setProfile(cpHigh);
        recorder.setOutputFile(PublicData.videoPath);
        recorder.setMaxDuration(30000); // 30 seconds
        recorder.setMaxFileSize(50000000); // Approximately 5 megabytes
    }

    private void prepareRecorder() {
        recorder.setPreviewDisplay(holder.getSurface());

        try {
            recorder.prepare();
           
        } catch (IllegalStateException e) {
            e.printStackTrace();
            finish();
        } catch (IOException e) {
            e.printStackTrace();
            finish();
        }
    }

    public void onClick(View v) {
    	
        if (recording) {
            recorder.stop();
            Intent myintent=new Intent(VideoCapture.this, SignatureAndVideoActivity.class).putExtra("checkvideo", "playvideo");
            PublicData.publicBundle.putString("checkvideo", "playvideo");
            myintent.putExtra("result", result);
            myintent.putExtra("resultp", resultp);
            startActivity(myintent);;
            recording = false;

            // Let's initRecorder so we can record again
            initRecorder();
            prepareRecorder();
           // Toast.makeText(getApplicationContext(), "tap to  stop", Toast.LENGTH_LONG).show();
        } else {
           
        	recording = true;
            recorder.start();
           // Toast.makeText(getApplicationContext(), "tap to stop", Toast.LENGTH_LONG).show();
        }
    }

    public void surfaceCreated(SurfaceHolder holder) {
        prepareRecorder();
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width,
            int height) {
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        if (recording) {
            recorder.stop();
            recording = false;
        }
        recorder.release();
        finish();
    }
}